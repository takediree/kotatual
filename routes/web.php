<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\RegisterController;
use App\Http\Controllers\KecamatanController;
use App\Http\Controllers\Kecamatan1Controller;
use App\Http\Controllers\UserController;
use App\Http\Controllers\User1Controller;
use App\Http\Controllers\MapController;
use App\Http\Controllers\BeritaController;
use App\Http\Controllers\KelurahanController;
use App\Http\Controllers\WisataController;
use App\Http\Controllers\WilayahController;
use App\Http\Controllers\SdaController;
use App\Http\Controllers\DesaController;
use App\Http\Controllers\PengurusController;
use App\Http\Controllers\SaranController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//user1

Route::get('/user1/struktur', [User1Controller::class, 'pengurus']);
Route::get('/user1/map', [User1Controller::class, 'map']);
Route::get('/user1/wilayah', [User1Controller::class, 'wilayah']);
Route::get('/user1/wisata', [User1Controller::class, 'wisata']);
Route::get('/user1/sda', [User1Controller::class, 'sda']);
Route::get('/user1/detail', [User1Controller::class, 'detail']);
Route::get('/user1/about', [User1Controller::class, 'about']);
Route::get('/user1/desa', [User1Controller::class, 'desa']);
Route::get('/user1/saran', [User1Controller::class, 'saran']);





// //user

Route::get('/admin/login', [LoginController::class, 'login'])->name('login');
Route::post('/admin/authenticate', [LoginController::class , 'authenticate']);
Route::get('/admin/register', [RegisterController::class, 'register']);
Route::post('/admin/register', [RegisterController::class, 'store']);
Route::get('admin/logout', [LoginController::class, 'logout']);





Route::post('/user1/saran', [SaranController::class, 'store']);

// //map
Route::get('/user/map', [MapController::class, 'index']);
Route::get('marker', [MapController::class, 'data']);







Route::get('/', [User1Controller::class, 'index']);

Route::group(['middleware' => 'auth'], function () {
    
    Route::get('/admin', [DashboardController::class, 'home']);
    
    Route::get('/user', [UserController::class, 'index']);
    Route::delete('/user/{user}', [UserController::class, 'destroy']);
    
    

//news
Route::get('/berita', [BeritaController::class, 'index']);
Route::get('/berita/tambah', [BeritaController::class, 'create']);
Route::post('/berita', [BeritaController::class, 'store']);
Route::delete('/berita/{berita}', [BeritaController::class, 'destroy']);
Route::get('/berita/edit/{berita}', [BeritaController::class, 'edit']);
Route::put('/berita/{berita}', [BeritaController::class, 'update']);

// desa
Route::get('/desa', [DesaController::class, 'index']);
Route::get('/desa/tambah', [DesaController::class, 'create']);
Route::post('/desa', [DesaController::class, 'store']);
//Route::get('/desa/show', [DesaController::class, 'show']);
Route::delete('/desa/{desa}', [DesaController::class, 'destroy']);
Route::get('/desa/edit/{desa}', [DesaController::class, 'edit']);
Route::put('/desa/{desa}', [DesaController::class, 'update']);

//wilayah
Route::get('/wilayah', [WilayahController::class, 'index']);
Route::get('/wilayah/tambah', [WilayahController::class, 'create']);
Route::post('/wilayah', [WilayahController::class, 'store']);
Route::delete('/wilayah/{wilayah}', [WilayahController::class, 'destroy']);
Route::get('/wilayah/edit/{wilayah}', [WilayahController::class, 'edit']);
Route::put('/wilayah/{wilayah}', [WilayahController::class, 'update']);


//wisata
Route::get('/wisata', [WisataController::class, 'index']);
Route::get('/wisata/tambah', [WisataController::class, 'create']);
Route::post('/wisata', [WisataController::class, 'store']);
Route::delete('/wisata/{wisata}', [WisataController::class, 'destroy']);
Route::get('/wisata/edit/{wisata}', [WisataController::class, 'edit']);
Route::get('/wisata/delete/{id}', [WisataController::class, 'delete']);
Route::put('/wisata/{wisata}', [WisataController::class, 'update']);


//sda
Route::get('/sda', [SdaController::class, 'index']);
Route::get('/sda/tambah', [SdaController::class, 'create']);
Route::post('/sda', [SdaController::class, 'store']);
Route::delete('/sda/{sda}', [SdaController::class, 'destroy']);
Route::get('/sda/edit/{sda}', [SdaController::class, 'edit']);
Route::put('/sda/{sda}', [SdaController::class, 'update']);


//Pengurus
Route::get('/pengurus', [PengurusController::class, 'index']);
Route::get('/pengurus/tambah', [PengurusController::class, 'create']);
Route::post('/pengurus', [PengurusController::class, 'store']);
Route::get('/pengurus/{pengurus}', [PengurusController::class, 'show']);
Route::delete('/pengurus/{pengurus}', [PengurusController::class, 'destroy']);
Route::get('/pengurus/{pengurus}/edit', [PengurusController::class, 'edit']);
Route::put('/pengurus/{pengurus}', [PengurusController::class, 'update']);

Route::get('/saran', [SaranController::class, 'index']);



});