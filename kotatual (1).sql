-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 04, 2021 at 12:35 AM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `kotatual`
--

-- --------------------------------------------------------

--
-- Table structure for table `berita`
--

CREATE TABLE `berita` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tanggal` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `judul` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `isi` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gambar` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `berita`
--

INSERT INTO `berita` (`id`, `tanggal`, `judul`, `isi`, `gambar`, `created_at`, `updated_at`) VALUES
(1, 'weq', 'penjarahan', 'wrqrqerqw', 'public/img/Sm4aRqIMTBzbA7d9S5eIB8TdRhbrtSnAsthfziUF.jpeg', '2020-11-28 00:53:21', '2020-11-28 10:47:46'),
(2, 'minggu 11 oktober', 'sndnjfhjsa', 'sfcxzcdfsv', 'public/img/vZFdyGff7K9kuAfan9jHacJz7loR5eDrLqXtbN44.jpeg', '2020-11-28 00:53:46', '2020-11-28 00:53:46'),
(3, 'selasa 19 oktober', 'asfqsf', 'asjdbagshdfasfdhvasdhasdsndkjaskbncmdnasbc', 'public/img/pDTEnpQ9k3Ip32H4aUhxUG5y7xIjQ25cWcMg7Wxg.jpeg', '2020-11-28 10:56:42', '2020-11-28 11:28:39'),
(4, 'selasa 19 oktober', 'dasd', 'dkjasgjdafshydfasjdgvashdgagshdjasghdjagsdagshdjagshdjagshdjagshd', 'public/img/DL5ehdlIeM1Eji0nSd716HY5TKDX7zKl9459qmHp.jpeg', '2020-11-28 11:27:53', '2020-11-28 11:28:18'),
(5, '2432423', 'dasd', 'nama saiusa djskafjlsa sfjalnfdsnfkjdsf jdfndskjfdsjlfn jdksfndjslf jdksfndslj jkfdnlsfn dskfjdkslfjkdslf jdkfjklsd jklkdsfndsl jfsndklfknlfdsfjdsf', 'public/img/HxqP317NwowHXrq4FTPLUcsOHQirqBzPIirmAOm5.jpeg', '2020-12-03 15:38:43', '2020-12-03 15:39:38');

-- --------------------------------------------------------

--
-- Table structure for table `desa`
--

CREATE TABLE `desa` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nama_desa` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `longitude` double NOT NULL,
  `latitude` double NOT NULL,
  `keterangan` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `gambar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `desa`
--

INSERT INTO `desa` (`id`, `nama_desa`, `longitude`, `latitude`, `keterangan`, `gambar`, `created_at`, `updated_at`) VALUES
(15, 'Dullah', -5.564177, 132.766974, 'Dullah adalah kelurahan di kecamatan Dullah Utara, Kota Tual, Provinsi Maluku, Indonesia.', 'public/img/pVTgPzoI2vjHojRWeOyPpkdgZNYzTG1Mv2g37I77.jpeg', '2020-12-03 16:27:36', '2020-12-03 16:27:36'),
(16, 'Dullah Laut', -5.5419, 132.719986, 'Dullah Laut adalah kelurahan di kecamatan Dullah Utara,\r\n Kota Tual, Provinsi Maluku, Indonesia.', 'public/img/X8AHLVmdyJJLJyoYBaBqHCfphC5GAaP3YZdkN5qB.jpeg', '2020-12-03 16:30:40', '2020-12-03 16:30:40'),
(17, 'Fiditan', -5.63792, 132.743225, 'Fiditan adalah kelurahan di kecamatan Dullah Utara, Kota Tual, Provinsi Maluku, Indonesia.', 'public/img/iOLWsjId4CREarzchLktOB242T5doxdYdHxSVFsV.jpeg', '2020-12-03 16:34:39', '2020-12-03 17:01:47'),
(18, 'Lebetawi', -5.538933, 132.780157, 'Lebetawi adalah kelurahan di kecamatan Dullah Utara, Kota Tual, Provinsi Maluku, Indonesia.', 'public/img/QbqdvTgVVkTh6GtyhIFKBjfLpbhVOko1nN82V4B2.jpeg', '2020-12-03 16:38:21', '2020-12-03 17:02:14'),
(19, 'Ohoitahit', -5.605607, 132.798137, 'Ohoitahit adalah kelurahan di kecamatan Dullah Utara, Kota Tual, Provinsi Maluku, Indonesia.', 'public/img/R0AZdRGXUFt3IxdcJx2ib81scwNr9Bz4hhmNbYwS.jpeg', '2020-12-03 16:40:46', '2020-12-03 17:02:35'),
(20, 'Ohoitel', -5.60896, 132.789488, 'Ohoitel  adalah kelurahan di kecamatan Dullah Utara, Kota Tual, Provinsi Maluku, Indonesia.', 'public/img/Cc5oJjGFosolCjOMwBR6uDKwwieukorxerfp6C77.jpeg', '2020-12-03 16:42:53', '2020-12-03 17:03:10'),
(21, 'Tamedan', -5.527763, 132.793263, 'Tamedan adalah kelurahan di kecamatan Dullah Utara, Kota Tual, Provinsi Maluku, Indonesia.', 'public/img/y5DMRpI3Rs7XtXjgjF93nCyxX8XhSx7VYGQMyQoo.jpeg', '2020-12-03 16:47:09', '2020-12-03 17:03:32'),
(22, 'Ngadi', -5.571054, 132.760044, 'Ngadi adalah kelurahan di kecamatan Dullah Utara, Kota Tual, Provinsi Maluku, Indonesia.', 'public/img/8rT5ODccC9lU9anEAUUKfLeX50Z16YlheA4e2oAj.jpeg', '2020-12-03 16:49:16', '2020-12-03 17:03:55'),
(25, 'eqwewq', 32123, 53435646, 'm,adsnas', NULL, '2021-01-28 13:27:51', '2021-01-28 13:27:51');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `kecamatan`
--

CREATE TABLE `kecamatan` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nama_kecamatan` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `longitude` double NOT NULL,
  `latitude` double NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `kelurahan`
--

CREATE TABLE `kelurahan` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nama_kelurahan` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `longitude` double NOT NULL,
  `latitude` double NOT NULL,
  `keterangan` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gambar` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2020_11_13_082548_create_kecamatan_table', 1),
(5, '2020_11_18_073242_create_berita_table', 1),
(6, '2020_11_19_210525_create_kelurahan_table', 1),
(7, '2020_11_19_214401_create_wilayah_table', 1),
(8, '2020_11_19_215307_create_wisata_table', 1),
(9, '2020_11_24_134930_create_desa_table', 1),
(10, '2020_11_24_190544_create_sda_table', 1),
(11, '2020_11_24_204441_create_pengurus_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pengurus`
--

CREATE TABLE `pengurus` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nama` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nip` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jabatan` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gambar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `pengurus`
--

INSERT INTO `pengurus` (`id`, `nama`, `nip`, `jabatan`, `gambar`, `created_at`, `updated_at`) VALUES
(17, 'ABDUL HARIS RENUAT, SH', '19791025 200501 1 011', 'Kepala Sub Bagian Keuangan', NULL, '2020-12-09 11:33:59', '2020-12-09 11:33:59'),
(18, 'IRFAN FADIRUBUN, S.Sos', '19791025 200501 1 011', 'CAMAT  P.  DULLAH   UTARA', NULL, '2020-12-09 14:01:42', '2020-12-09 14:01:42'),
(19, 'AMIER RENGUR, S.STP', '19810427 200112 1 002', 'Kepala Seksi PMD', NULL, '2020-12-09 14:02:40', '2020-12-09 14:02:40'),
(20, 'MUH SAID UBRUSUN, SE', '19761220 201001 1 011', 'Kepala Seksi Pelayanan Umum', NULL, '2020-12-09 14:03:24', '2020-12-09 14:03:24'),
(21, 'AHMAD TAMHER, S.Sos', '19750315 201001 1 016', 'Kepala Seksi Trantib', NULL, '2020-12-09 14:03:51', '2020-12-09 14:03:51'),
(22, 'NOVITA MILANY RENUAT', '19741130 200012 2 004', 'Kepala Sub Bagian Keuangan', NULL, '2020-12-09 14:04:17', '2020-12-09 14:04:17'),
(23, 'PAULUS WILLIEM RAHAWARIN, S.Par', '19821122 201001 1 020', 'Kepala Sub Bagian Umum dan Kepegawaian', NULL, '2020-12-09 14:04:44', '2020-12-09 14:04:44'),
(24, 'DIEN FAHRIZA MADUBUN, S.Kom', '19841101 201503 2 002', 'Kepala Sub Bagian Perencanaan', NULL, '2020-12-09 14:05:10', '2020-12-09 14:05:10'),
(25, 'ABDUL MAJID TAMHER', '19681209 199303 1 008', 'Fungsional Umum', NULL, '2020-12-09 14:05:37', '2020-12-09 14:05:37'),
(26, 'BASRI RENWARIN', '19680111 200112 1 003', 'Fungsional Umum', NULL, '2020-12-09 14:06:08', '2020-12-09 14:06:08'),
(27, 'LAILAN HUSAID RENWARIN', '19800124 201111 2 004', 'Fungsional Umum', NULL, '2020-12-09 14:06:36', '2020-12-09 14:06:36'),
(28, 'MOHAMAD SAID RUMADAN', '19901227 201001 1 001', 'Fungsional Umum', NULL, '2020-12-09 14:07:10', '2020-12-09 14:07:10'),
(29, 'aaaaaaaaaa', '19681209 199303 1 008', 'CAMAT  P.  DULLAH   UTARA', NULL, '2021-01-28 13:29:22', '2021-01-28 13:29:22');

-- --------------------------------------------------------

--
-- Table structure for table `saran`
--

CREATE TABLE `saran` (
  `id` int(11) NOT NULL,
  `tanggal` timestamp NOT NULL DEFAULT current_timestamp(),
  `nama` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `saran` text NOT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `saran`
--

INSERT INTO `saran` (`id`, `tanggal`, `nama`, `email`, `saran`, `updated_at`, `created_at`) VALUES
(1, '2020-12-05 22:08:55', '121312', 'asu@gmail', 'dsfsfdsfdsfdsfds', '2020-12-05 14:08:55', '2020-12-05 14:08:55'),
(2, '2020-12-05 22:12:39', 'wqjer83w', 'aku@gmail', 'sadsafasfwdgdgds', '2020-12-05 14:12:39', '2020-12-05 14:12:39'),
(3, '2020-12-05 22:16:27', '121312', 'tak@gmail.com', 'savdganvdghasvdnasvdjasvdj svjadgcakjbdkjggfhsdfbhd fagfgshfask\r\nhsdagdkjas\r\njasdhkjashfkashfskjhfjdgsfdsjf\r\nfhdsgfdshmfgk', '2020-12-05 14:16:27', '2020-12-05 14:16:27'),
(4, '2020-12-06 17:24:03', 'samsir', 'thetamora.net@gmail.com', 'DSAF,DNGJDF NGKJDFHBGVSKJHFLKSNBFVSHFKVSJBHFVBSKJHFSKDHVBFDSKJ', '2020-12-06 09:24:03', '2020-12-06 09:24:03'),
(5, '2020-12-06 17:25:10', 'samsir', 'thetamora.net@gmail.com', 'WDAFASFASDFSADAS', '2020-12-06 09:25:10', '2020-12-06 09:25:10'),
(6, '2020-12-06 17:25:39', 'ascas', 'thetamora.net@gmail.com', 'DQDADSADFWAEWW3EWQDWA', '2020-12-06 09:25:39', '2020-12-06 09:25:39'),
(7, '2020-12-06 17:26:28', 'vsd', 'dhyrtbahang@gmail.com', 'DSADASDADASDSADSA', '2020-12-06 09:26:28', '2020-12-06 09:26:28'),
(8, '2020-12-06 21:37:50', 'surti', 'surti@gmail.com', 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Harum distinctio veniam odit vel aliquid. Sint corporis sequi ipsam expedita! Aut aliquam quos omnis voluptas, eius incidunt hic ab aliquid recusandae!', '2020-12-06 13:37:50', '2020-12-06 13:37:50');

-- --------------------------------------------------------

--
-- Table structure for table `sda`
--

CREATE TABLE `sda` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nama` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_desa` int(11) NOT NULL,
  `longitude` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `latitude` double NOT NULL,
  `keterangan` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `gambar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sda`
--

INSERT INTO `sda` (`id`, `nama`, `id_desa`, `longitude`, `latitude`, `keterangan`, `gambar`, `created_at`, `updated_at`) VALUES
(10, 'rumput laut', 16, '-5.536777523599338', 132.72229194600922, 'Rumput laut tergolong tanaman berderajat\r\nrendah, umumnya tumbuh melekat pada substrat\r\ntertentu, tidak mempunyai akar, batang maupun\r\ndaun sejati. Tapi hanya mempunyai batang yang\r\ndisebut thallus. Rumput laut tumbuh di alam\r\ndengan melekatkan dirinya pada karang, lumpur,\r\npasir, batu dan benda keras lainnya. Rumput laut\r\nyang di budidayakan di Kota Tual adalah rumput\r\nlaut jenis Eucheuma cottonii, dimana rumput laut\r\njenis ini memerlukan sinar matahari untuk proses\r\nfotosintesis, sehingga rumput laut ini hanya akan\r\nhidup pada kedalaman sejauh sinar matahari\r\nmasih mampu mencapainya.', 'public/img/aFKPw501SqONwF4wlcz5PNiZqDK8rKeHMwOYf2kM.jpeg', '2021-01-08 06:58:56', '2021-01-08 06:58:56'),
(11, '121312', 18, '32123', 213123, 'ksdlasdjas', 'public/img/pchr8FYDf5WienySRauRCGdALBPywuGoo2vD1oYW.jpeg', '2021-01-28 13:30:14', '2021-01-28 13:30:14');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(4, 'tak@gmail.com', 'tak@gmail.com', NULL, '$2y$10$V348RcYaO/.gkJa7K0Y3heLi5kZrgClR2UYPF/NKx86xwDyTDLf1G', NULL, '2020-12-03 06:56:12', '2020-12-03 06:56:12'),
(8, 'asu@gmail', 'asu@gmail', NULL, '$2y$10$MeBsnUYYe49G7cEGnz0vJecgnp4AnCJWzUoIXkxJquHVZas.aUfp2', NULL, '2020-12-04 07:51:58', '2020-12-04 07:51:58'),
(9, 'judul', 'j@gmail', NULL, '$2y$10$deFb6qIEcr4D0bdeCm5tuOpEgSKlW5/./qVOCWQmUsKBf1zxQ839G', NULL, '2020-12-04 08:00:28', '2020-12-04 08:00:28'),
(10, 'ical@gmail', 'ical@gmail', NULL, '$2y$10$17mp9Wr79ybyOi0MFDKRk.g.QmNYq9wAeQt3BAY8EBh3uFzcVpDD2', NULL, '2020-12-05 07:44:54', '2020-12-05 07:44:54');

-- --------------------------------------------------------

--
-- Table structure for table `wilayah`
--

CREATE TABLE `wilayah` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nama` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_desa` int(11) NOT NULL,
  `longitude` double NOT NULL,
  `latitude` double NOT NULL,
  `keterangan` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gambar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wilayah`
--

INSERT INTO `wilayah` (`id`, `nama`, `id_desa`, `longitude`, `latitude`, `keterangan`, `gambar`, `created_at`, `updated_at`) VALUES
(9, 'Pegunungan', 15, -5.564476, 132.766969, 'wilayah pegunungan terbesar di wilayah kota Tual', 'public/img/caOorrkQ2VY2UFf7MlatXgekhXlnW5GIptko444m.jpeg', '2020-12-03 14:31:28', '2020-12-03 15:32:58'),
(11, 'bakau', 11, 321213, 312312312, 'dasdasdjkasfnjas', 'public/img/yLqcJOQ7HauiVXHX5HUtvlRXOyzM7CWgS9vUnuN6.jpeg', '2020-12-03 14:35:01', '2020-12-03 14:35:01');

-- --------------------------------------------------------

--
-- Table structure for table `wisata`
--

CREATE TABLE `wisata` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nama_wisata` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_desa` int(11) NOT NULL,
  `longitude` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `latitude` double NOT NULL,
  `keterangan` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `gambar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wisata`
--

INSERT INTO `wisata` (`id`, `nama_wisata`, `id_desa`, `longitude`, `latitude`, `keterangan`, `gambar`, `created_at`, `updated_at`) VALUES
(37, 'pantai difur', 21, '-5.543191968759027', 132.7984042235259, 'Wisata pantai dan\r\npemandangan alam\r\npohon beringin', 'public/img/rifsYaEO6rGWO6wIGUrsk9lvGQY1L4lZ95IXLgjZ.jpeg', '2020-12-10 21:22:24', '2020-12-10 21:24:47'),
(38, 'Pantai Nam Indah', 19, '-5.548743785646943', 132.80855846553467, 'Terletak di Desa Ohoitahit Kecamatan Dullah Utara kota Tual. Dapat ditempuh ± 40 menit dengan kendaraan roda 2 (dua) dan roda 4 (empat). Pantai ini memiliki panjang pasir sekitar 200 meter dengan pasir putih halus. Sangat cocok untuk untuk bersantai karena memiliki suasana tenang, nyaman dan sejuk dengan pepohonan yang rindang. Pantai ini berhadapan dengan pantai difur. Pantai ini sangat cocok bagi wisatawan yang ingin bersantai karena memiliki suasana sepi, nyaman dan sejuk dengan pohon-pohon. Fasiliatas seperti tempat bersantai (shelter), panggung, dermaga kayu ada dipantai ini.', 'public/img/Kw8WU7TSq7wJI6FHRIAOdqjide2HAkH9rhdaTUlm.jpeg', '2020-12-10 21:37:39', '2020-12-10 21:37:39'),
(39, 'Pulau Bair', 16, '-5.438366675577377', 132.70312717366684, 'Untuk menuju Pulau Bair cukup mudah. Dimulai dengan perjalanan darat dari pusat Kota Tual menuju Desa Dullah Darat dengan jarak tempuh kurang lebih 30 menit dengan menggunakan kendaraan roda 2 atau 4. Kita juga bisa menggunakan angkutan umum menuju Desa Dullah dari terminal angkutan umum yang ada di pelabuhan kapal Kota Tual. Setelah sampai di pelabuhan/dermaga Dullah Darat kita lanjutkan dengan perjalanan laut, menggunakan perahu speed yang harus kita sewa karena lokasi Pulau Bair terletak diujung dari deretan pulau - pulau yang ada di Desa Dullah Laut. Waktu tempuh kurang lebih 1 jam perjalanan.', 'public/img/VaWf6qO4swjqmf687iTTurgY1VhqccQpVg5QdwxC.jpeg', '2020-12-10 21:56:00', '2020-12-10 21:56:00'),
(40, 'Danau ngadi', 22, '-5.573714414855', 132.76429366785652, 'Danau Ngadi terletak di Desa Ngadi Kecamatan Dullah Utara. Perjalanan menuju destinasi ini hanya membutuhkan waktu paling lama 30 menit dari Kota Tual. Danau Ngadi masih dikitari dengan hutan yang lebat yang ditubuhi beraneka pohon yang rindang dan dijaga masyarakat sekitarnya sebagai hutan lindung. Setelah sampai di kawasan wisata ini, para pengunjung akan disuguhi pemandangan yang indah berupa pepohonan sepanjang danau yang hijau serta jernihnya air danau yang memanjakan mata. Bagi penyuka dunia fotografi, disinilah surganya. Selain danau dan pepohonan, pohon-pohon kering ataupun yang tumbang dapat memanjakan hobi yang satu ini. Danau ini juga sebagai sumber air bersih bagi masyakat sekitar.', 'public/img/FWBL0cjFryAaE6HZYGiD3k6PZBAExZ4fyLes9ehI.jpeg', '2021-01-08 04:43:34', '2021-01-08 04:46:02'),
(41, 'pulau adranan', 16, '-5.516323844457308', 132.7500771952001, 'Terletak di sebelah timur Desa Dullah Laut, Kecamatan Dullah Utara Kota Tual, Luas pulau ± 500 M2. Berjarak ± 30 menit dari Kota Tual, Dapat ditempuh dengan kendaraan laut/speedboat ± 15 menit dari Desa Dullah.\r\nMemiliki daya tarik pantai dengan pasir putih, air laut yang jernih dan terumbu karang yang indah sehingga cocok untuk mancing, Snorkeling, diving dan mandi-mandi.\r\nkondisi air laut yang jernih dapat melihat langsung terumbu karang. daya tarik lain dari pulau adranan adalahperubahan musim timur dan barat dimana pasir pantai akan bergeser ketimur begitu pula sebaliknya', 'public/img/zv77fTvAaOAtS4wJs8xlq73ODvcLA0oa8sVgqNKO.jpeg', '2021-01-08 05:00:39', '2021-01-08 05:00:39'),
(42, 'danau tahit ko', 16, '-5.538015684598059', 132.69245368310126, 'Terletak di sebelah barat ±500 meter dari Dusun Duroa Desa Dullah Laut kecamatan Dullah Utara Kota Tual. Jarak ± 30 menit dari Kota Tual dan dapat ditempuh dengan kendaraan laut/speedboat dari Kota Tual. Memiliki pepohonan rindang sekitar danau, suasana tenang dengan kicauan burung dan dapat digunakan sebagai lokasi pemancingan. Karakteristik danau ini yang mengandung air laut dan memiliki daya tarik tersendiri.', 'public/img/xtMhbCanuYP1vz19ARWUwTmEFEe3PD5gCR3mKMhx.jpeg', '2021-01-08 05:11:52', '2021-01-08 05:11:52'),
(43, 'pulau ohoimas', 16, '-5.438450953497085', 132.71762078538598, 'Pulau kecil yang indah dengan pasir putih dan bentangan laut termasuk dalam kawasan Desa Dullah Laut disebut juga pulau burung karena banyak burung yang tinggal di pulau ini dan sering menjadi tempat bersinggah setelah kunjungan dari Pulau bair karena lokasinya yang terdekat dengan daratan.', 'public/img/RlL7Y8mhRBbw3Eh0O6MskRt3ft2eJqNVtpnaZaTH.jpeg', '2021-01-08 05:26:50', '2021-01-08 05:26:50');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `berita`
--
ALTER TABLE `berita`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `desa`
--
ALTER TABLE `desa`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `kecamatan`
--
ALTER TABLE `kecamatan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kelurahan`
--
ALTER TABLE `kelurahan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `pengurus`
--
ALTER TABLE `pengurus`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `saran`
--
ALTER TABLE `saran`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sda`
--
ALTER TABLE `sda`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `wilayah`
--
ALTER TABLE `wilayah`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wisata`
--
ALTER TABLE `wisata`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `berita`
--
ALTER TABLE `berita`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `desa`
--
ALTER TABLE `desa`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `kecamatan`
--
ALTER TABLE `kecamatan`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `kelurahan`
--
ALTER TABLE `kelurahan`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `pengurus`
--
ALTER TABLE `pengurus`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `saran`
--
ALTER TABLE `saran`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `sda`
--
ALTER TABLE `sda`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `wilayah`
--
ALTER TABLE `wilayah`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `wisata`
--
ALTER TABLE `wisata`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
