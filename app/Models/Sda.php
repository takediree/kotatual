<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Sda extends Model
{
    use HasFactory;

    protected $table = 'sda';
    protected $fillable = ['nama', 'longitude', 'latitude','keterangan'];
}
