<?php

namespace App\Http\Controllers;

use App\Models\Wisata;
use App\Models\Desa;
use App\Models\User;
use Illuminate\Http\Request;

class WisataController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        

        $wisata = Wisata::all();
        return view('wisata.index', compact('wisata'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $desa = Desa::all();
        return view('wisata.tambah',compact('desa'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $request->validate([
            'nama_wisata' => 'required',
            'id_desa' => 'required',
            'longitude' => 'required|numeric',
            'latitude' => 'required |numeric',
            'keterangan' => 'required',
            'gambar' => 'image|mimes:jpg,png,jpeg,|max:2048',
        ]);



        $wisata = new Wisata;


        if($request->file('gambar') != null){
        $path = $request->file('gambar')->store('public/img');
        
        $wisata->gambar = $path;
        }
       $wisata->nama_wisata = $request->nama_wisata;
       $wisata->id_desa = $request->id_desa;
       $wisata->longitude = $request->longitude;
       $wisata->latitude = $request->latitude;
       $wisata->keterangan = $request->keterangan;
       
       $wisata->save();
        return redirect('wisata')->with('status', 'Data berhasil ditambahkan!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Wisata  $wisata
     * @return \Illuminate\Http\Response
     */
    public function show(Wisata $wisata)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Wisata  $wisata
     * @return \Illuminate\Http\Response
     */
    public function edit(Wisata $wisata)
    {
        //
        $desa = Desa::all();
        return view('wisata.edit', compact('wisata','desa'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Wisata  $wisata
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Wisata $wisata)
    {
        $request->validate([
            'nama_wisata' => 'required',
            'id_desa' => 'required',
            'longitude' => 'required|numeric',
            'latitude' => 'required |numeric',
            'keterangan' => 'required',
            'gambar' => 'required|image|mimes:jpg,png,jpeg,|max:2048',
        ]);

        
        $wisata = Wisata::find($wisata->id);
        if($request->file('gambar') != null){
            
            $path = $request->file('gambar')->store('public/img');

            $wisata->gambar= $path;

        }
            $wisata->nama_wisata = $request->nama_wisata;
            $wisata->id_desa = $request->id_desa;
            $wisata->longitude = $request->longitude;
            $wisata->latitude = $request->latitude;
            $wisata->keterangan = $request->keterangan;

            $wisata->save();
            return redirect('wisata')->with('status', 'Data berhasil diubah!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Wisata  $wisata
     * @return \Illuminate\Http\Response
     */
    public function destroy($wisata)
    {
        //

        dd($wisata);
        
        $w = Wisata::find($wisata);
        $w->delete();
        return redirect('wisata')->with('status', 'Data berhasil dihapus!');
    }

    public function delete($id)
    {
        //
 
        $w = Wisata::find($id);
        $w->delete();
        return redirect('wisata')->with('status', 'Data berhasil dihapus!');
    }
}
