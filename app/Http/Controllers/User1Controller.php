<?php

namespace App\Http\Controllers;

use App\Models\User1;
use App\Models\Berita;
use App\Models\Desa;
use App\Models\Pengurus;
use App\Models\Wilayah;
use App\Models\Wisata;
use App\Models\Sda;
use Illuminate\Http\Request;

class User1Controller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $berita = Berita::all();
        $pengurus = Pengurus::all();
        $desa = Desa::all();

        return view('index',(compact('berita' , 'desa','pengurus')));
    }
    public function pengurus()
    {
        //
        $pengurus = Pengurus::all();
        $desa = Desa::all();
        return view('user1/struktur',compact('desa','pengurus'));
    }
    public function map()
    {
        //
        $desa = Desa::all();
        
        $wisata = Wisata :: all();
        $sda = Sda :: all();

        return view('user1/map',compact('desa','wisata','sda'));
    }
    public function wilayah(Request $request)
    {
        $id = $request->get("id");

        $desa = Desa::all();
        $berita = Berita::all();
        $wilayah = Wilayah::all();

        $potensiWisata = Wisata::where('id_desa' , '=' , $id)->get();
        $potensiWilayah = Wilayah::where('id_desa' , '=' , $id)->get();
        $potensiSda = Sda::where('id_desa' , '=' , $id)->get();

        return view('user1/wilayah',compact('wilayah','desa','potensiWisata','potensiWilayah','potensiSda'));
    }

    public function wisata(Request $request)
    {
        $id = $request->get("id");

        $desa = Desa::all();
        $berita = Berita::all();
        $wisata = Wisata::all();


        $potensiWisata = Wisata::where('id_desa' , '=' , $id)->get();
        return view('user1/wisata',compact('desa','berita','wisata','potensiWisata'));
    }

    public function sda(Request $request)
    {
        $id = $request->get("id");
        $desa = Desa::all();
        $berita = Berita::all();
        $sda = Sda::all();

        $potensiSda = Sda::where('id_desa' , '=' , $id)->get();
        return view('user1/sda',compact('berita','desa','sda','potensiSda'));
    }

    public function detail(Request $request)
    {
        //
        $id = $request->get("id");

        $desa = Desa :: all();
        $wilayah= Wilayah::all();
        $wisata = Wisata::all();
        $sda = Sda::all();
        $detailwilayah = Wilayah :: find($id);
        $detailwisata = Wisata :: find($id);
        $dsda = Sda :: all();

        

      
        return view('user1/detail',compact('desa','wilayah','wisata','detailwisata','detailwilayah'));
    }
    
    public function about()
    {
        //
        $pengurus = Pengurus::all();
        $desa = Desa::all();
        return view('user1/about',compact('desa','pengurus'));
    }



    public function desa(Request $request)
    {
        //
    
        $id = $request->get("id");

        $desa = Desa::all();

        $detailDesa = Desa::find($id);


        $potensiWisata = Wisata::where('id_desa' , '=' , $id)->get();
        $potensiWilayah = Wilayah::where('id_desa' , '=' , $id)->get();
        $potensiSda = Sda::where('id_desa' , '=' , $id)->get();

        return view('user1/desa',compact('desa', 'detailDesa','potensiWisata','potensiWilayah','potensiSda'));
    }
    
    public function saran()
    {
        $desa = Desa::all();


        return view('user1/saran',compact('desa'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\User1  $user1
     * @return \Illuminate\Http\Response
     */
    public function show(User1 $user1)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\User1  $user1
     * @return \Illuminate\Http\Response
     */
    public function edit(User1 $user1)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\User1  $user1
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User1 $user1)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\User1  $user1
     * @return \Illuminate\Http\Response
     */
    public function destroy(User1 $user1)
    {
        //
    }
}
