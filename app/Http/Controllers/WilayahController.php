<?php

namespace App\Http\Controllers;

use App\Models\Wilayah;
use App\Models\Desa;
use Illuminate\Http\Request;

class WilayahController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $wilayah = Wilayah::all();
        
        return view('wilayah.index', compact('wilayah'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $desa= Desa::all();
        return view('wilayah.tambah',compact('desa'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       $path = $request->file('gambar')->store('public/img');

       $wilayah = new Wilayah;

       $wilayah->nama = $request->nama;
       $wilayah->id_desa = $request->id_desa;
       $wilayah->longitude = $request->longitude;
       $wilayah->latitude = $request->latitude;
       $wilayah->keterangan = $request->keterangan;
       
       $wilayah->gambar = $path;
       $wilayah->save();

        return redirect('wilayah')->with('status', 'Data berhasil ditambahkan!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Wilayah  $wilayah
     * @return \Illuminate\Http\Response
     */
    public function show(Wilayah $wilayah)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Wilayah  $wilayah
     * @return \Illuminate\Http\Response
     */
    public function edit(Wilayah $wilayah)
    {
        //
        $desa= Desa::all();
        return view('wilayah.edit', compact('wilayah','desa'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Wilayah  $wilayah
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Wilayah $wilayah)
    {
        //
        $wilayah = Wilayah::find($wilayah->id);
        
        if($request->file('gambar') != null){
            
            $path = $request->file('gambar')->store('public/img');

            $wilayah->gambar= $path;

        }
            $wilayah->nama = $request->nama;
            $wilayah->id_desa = $request->id_desa;
            $wilayah->longitude = $request->longitude;
            $wilayah->latitude = $request->latitude;
            $wilayah->keterangan = $request->keterangan;

            $wilayah->save();

            return redirect('wilayah')->with('status', 'Data berhasil diedit!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Wilayah  $wilayah
     * @return \Illuminate\Http\Response
     */
    public function destroy(Wilayah $wilayah)
    {
        //
        $wilayah->delete();
        return redirect('wilayah')->with('status', 'Data berhasil dihapus!');
    }
}
