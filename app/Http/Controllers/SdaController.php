<?php

namespace App\Http\Controllers;

use App\Models\Sda;
use App\Models\Desa;
use Illuminate\Http\Request;

class SdaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $sda = Sda::all();
        return view('sda.index', compact('sda'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       $desa = Desa::all();
        return view('sda.tambah',compact('desa'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required',
            'id_desa' => 'required',
            'longitude' => 'required|numeric',
            'latitude' => 'required |numeric',
            'keterangan' => 'required',
            'gambar' => 'required|image|mimes:jpg,png,jpeg,|max:2048',
        ]);
        $path = $request->file('gambar')->store('public/img');

        $sda = new Sda;
 
        $sda->nama = $request->nama;
        $sda->id_desa = $request->id_desa;
        $sda->longitude = $request->longitude;
        $sda->latitude = $request->latitude;
        $sda->keterangan = $request->keterangan;
        
        $sda->gambar = $path;
        $sda->save();
 
         return redirect('sda')->with('status', 'Data berhasil ditambahkan!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Sda  $sda
     * @return \Illuminate\Http\Response
     */
    public function show(Sda $sda)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Sda  $sda
     * @return \Illuminate\Http\Response
     */
    public function edit(Sda $sda)
    {
        $desa = Desa::all();
        return view('sda.edit', compact('sda','desa'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Sda  $sda
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Sda $sda)
    {
        $request->validate([
            'nama' => 'required',
            'id_desa' => 'required',
            'longitude' => 'required|numeric',
            'latitude' => 'required |numeric',
            'keterangan' => 'required',
            'gambar' => 'required|image|mimes:jpg,png,jpeg,|max:2048',
        ]);
        $sda = Sda::find($sda->id);
        if($request->file('gambar') != null){
            
            $path = $request->file('gambar')->store('public/img');

            $sda->gambar= $path;

        }
            $sda->nama = $request->nama;
            $sda->id_desa = $request->id_desa;
            $sda->longitude = $request->longitude;
            $sda->latitude = $request->latitude;
            $sda->keterangan = $request->keterangan;

            $sda->save();

            return redirect('sda')->with('status', 'Data berhasil diedit!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Sda  $sda
     * @return \Illuminate\Http\Response
     */
    public function destroy(Sda $sda)
    {
        $sda->delete();
        
        
        return redirect('sda')->with('status', 'Data berhasil dihapus!');
    }
}
