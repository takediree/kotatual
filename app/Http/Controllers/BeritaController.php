<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Berita;

class BeritaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $berita = Berita::all();
        return view('berita.index', ['berita' => $berita]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('berita.tambah');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $path = $request->file('gambar')->store('public/img');
        
        $berita = new Berita;

        $berita->tanggal = $request->tanggal;
        $berita->judul = $request->judul;
        $berita->isi = $request->isi;
        
        $berita->gambar = $path;

        $berita->save();

        return redirect('berita')->with('status', 'Data berhasil ditambahkan!');
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    public function edit( Berita $berita)
    {

        return view('berita.edit',compact('berita'));

    }

     public function update(Request $request, Berita $berita)
    {


        
        $berita = Berita::find($berita->id);

        if($request->file('gambar') != null){
        
            $path = $request->file('gambar')->store('public/img');
        
            $berita->gambar = $path;
        
        }

            
        $berita->tanggal = $request->tanggal;
        $berita->judul = $request->judul;
        $berita->isi = $request->isi;
        

        $berita->save();

        return redirect('berita')->with('status', 'Data berhasil update!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Berita $berita)
    {
        $berita->delete();
        return redirect('berita')->with('status', 'Data Berhasil Dihapus');
    }
}
