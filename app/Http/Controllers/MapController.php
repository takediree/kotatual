<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class MapController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //

        $data = array();

        $data[] = ["nama" => "hello", "lat" => "132.757006", "lang" => "-5.624113"];

        $data[] = ["nama" => "hello2", "lat" => "132.753970", "lang" => "-5.630557"];


        $potensiWisata = Wisata::all();
        dd($potensiWisata);
        return view('user.map', compact("data"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function data()
    {
        // data marker
        $markers = array(
            array(
                'id' => 1,
                'title' => 'Marker 1',
                'desc' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmodtempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
                'lat' => '-7.644871999999999',
                'lng' => '112.90329700000007'
            ),
            array(
                'id' => 2,
                'title' => 'Marker 2',
                'desc' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmodtempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
                'lat' => '-6.188597',
                'lng' => '106.58851700000002'
            ),
            array(
                'id' => 3,
                'title' => 'Marker 3',
                'desc' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmodtempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
                'lat' => '-7.981894',
                'lng' => '112.62650299999996'
            ),
            array(
                'id' => 4,
                'title' => 'Marker 4',
                'desc' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmodtempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
                'lat' => '-6.202251',
                'lng' => '107.001587'
            )
        );

        // set response
        $response = array(
            'type' => 'FeatureCollection',
            'features' => array()
        );

        // loop marker
        foreach ($markers as $key => $val) {
            $id = (int) $val['id'];
            $title = $val['title'];
            $desc = $val['desc'];
            $lat = $val['lat'];
            $lng = $val['lng'];

            // set properties
            $properties = array(
                'id' => $id,
                'title' => $title,
                'desc' => $desc,
            );

            // push data to features
            array_push($response['features'], array(
                'type' => 'Feature',
                'geometry' => array(
                    'type' => 'Point',
                    'coordinates' => array($lng, $lat)
                ),
                'properties' => $properties
            ));
        }

        // set response JSON
        header('Content-Type: application/json');
        echo json_encode($response);
    }
}
