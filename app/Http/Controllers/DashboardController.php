<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Wisata;
use App\Models\Desa;
use App\Models\Sda;
use App\Models\User;
use App\Models\Pengurus;

class DashboardController extends Controller
{
    //
    public function home()
    {
        $desa = Desa::all();
        $wisata = Wisata::all();
        $sda = Sda::all();
        $pengurus = Sda::all();
        $jumlahWisata = Wisata::count();
        $jumlahSda = Sda::count();
        $jumlahDesa = Desa::count();
        $jumlahPengurus = Pengurus::count();
        
        return view('admin/index', compact('desa', 'wisata','sda','jumlahWisata','pengurus','jumlahPengurus','jumlahDesa','jumlahSda'));
    }
}
