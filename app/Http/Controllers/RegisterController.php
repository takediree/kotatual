<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;

class RegisterController extends Controller
{
    //
    public function register()
    {
        return view('admin/register');
    }

    public function store(Request $request){

        $user = new User();

        $user->name = $request->email;
        $user->email = $request->email;
        $user->password = bcrypt($request->password);

        $user->save();

        return redirect()->to('/user');


    }

    public function login(Request $request){



    }
}
