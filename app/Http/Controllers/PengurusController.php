<?php

namespace App\Http\Controllers;

use App\Models\Pengurus;
use Illuminate\Http\Request;

class PengurusController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pengurus = Pengurus::all();
        return view('pengurus.index', compact('pengurus')); 
    }
    

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('pengurus.tambah');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request->validate([
            'nama' => 'required',
            'nip' => 'required',
            'jabatan' => 'required',
            'gambar' => 'null',
           
            ]);
        $pengurus = new Pengurus;
        if($request->file('gambar') != null){
        
        $path = $request->file('gambar')->store('public/img');
        
        $pengurus->gambar = $path;
        }
       $pengurus->nama = $request->nama;
       
       $pengurus->nip = $request->nip;
       $pengurus->jabatan = $request->jabatan;
       
       $pengurus->save();

        return redirect('pengurus')->with('status', 'Data berhasil ditambahkan!');
    
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Pengurus  $pengurus
     * @return \Illuminate\Http\Response
     */
    public function show(Pengurus $pengurus)
    {
        //
        return view('pengurus.show',compact('pengurus'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Pengurus  $pengurus
     * @return \Illuminate\Http\Response
     */
    public function edit(Pengurus $pengurus)
    {
        //
        return view('pengurus.edit',compact('pengurus'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Pengurus  $pengurus
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Pengurus $pengurus)
    {

        $pengurus = Pengurus::find($pengurus->id);
        if($request->file('gambar') != null){
        
            $path = $request->file('gambar')->store('public/img');
            
            $pengurus->gambar = $path;
        }
       $pengurus->nama = $request->nama;
       $pengurus->nip = $request->nip; 
       $pengurus->jabatan = $request->jabatan;
       $pengurus->save();

        return redirect('pengurus')->with('status', 'Data berhasil diubah!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Pengurus  $pengurus
     * @return \Illuminate\Http\Response
     */
    public function destroy(Pengurus $pengurus)
    {
        //
        $pengurus->delete();
        return redirect('pengurus')->with('status', 'Data Berhasil Dihapus'); 
    }
}
