<?php

namespace App\Http\Controllers;

use App\Models\Desa;
use Illuminate\Http\Request;

class DesaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $desa = Desa::all();
        return view('desa.index', compact('desa'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('desa.tambah');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request->validate([
            'nama_desa' => 'required',
           
            'longitude' => 'required|numeric',
            'latitude' => 'required |numeric',
            'keterangan' => 'required',
            'gambar' => 'image|mimes:jpg,png,jpeg,|max:2048',
            
        ]);

        $desa = new Desa;


        if($request->file('gambar','geo_json') != null){
        $path = $request->file('gambar')->store('public/img');
        $pathh = $request->file('geo_json')->store('public/geojson');
        $desa->gambar = $path;
        $desa->geo_json = $pathh;
        }
        $desa->nama_desa = $request->nama_desa;
        $desa->longitude = $request->longitude;
        $desa->latitude = $request->latitude;
        $desa->keterangan = $request->keterangan;;
       
       $desa->save();
        return redirect('desa')->with('status', 'Data berhasil ditambahkan!');
    }
    

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Desa  $desa
     * @return \Illuminate\Http\Response
     */
    public function show(Desa $desa)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Desa  $desa
     * @return \Illuminate\Http\Response
     */
    public function edit(Desa $desa)
    {
        //
        return view('desa.edit', compact('desa'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Desa  $desa
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Desa $desa)
    {
        //
        $request->validate([
            'nama_desa' => 'required',
            
            'longitude' => 'required|numeric',
            'latitude' => 'required |numeric',
            'keterangan' => 'required',
            'gambar' => 'image|mimes:jpg,png,jpeg,|max:2048',
        ]);
        $desa = Desa::find($desa->id);
        if($request->file('gambar','geo_json') != null){
        
            $path = $request->file('gambar')->store('public/img');
            $pathh = $request->file('geo_json')->store('public/geojson');
            $desa->gambar =$path;
            $desa->geo_json =$pathh;

        }

        $desa->nama_desa = $request->nama_desa;
        $desa->longitude = $request->longitude;
        $desa->latitude = $request->latitude;
        $desa->keterangan = $request->keterangan;

        $desa->save();

        return redirect('desa')->with('status', 'Data berhasil update!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Desa  $desa
     * @return \Illuminate\Http\Response
     */
    public function destroy(Desa $desa)
    {
        //
        $desa->delete();
        return redirect('desa')->with('status', 'Data Berhasil Dihapus'); 
    }
}
