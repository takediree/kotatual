<aside class="main-sidebar sidebar-dark-primary elevation-4">
  <!-- Brand Logo -->
  <a href="index3.html" class="brand-link">
    <!-- <img src="dist/img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8"> -->
    <span class="brand-text font-weight-light">Web GIS</span>
  </a>

  <!-- Sidebar -->
  <div class="sidebar">
    <!-- Sidebar user panel (optional) -->
    <div class="user-panel mt-3 pb-3 mb-3 d-flex">
      <div class="image">
        <!-- <img src="dist/img/user2-160x160.jpg" class="img-circle elevation-2" alt="User Image"> -->
      </div>
      <div class="info">
        <a href="#" class="d-block"> {{session('email')}}</a>
      </div>
    </div>

    <!-- Sidebar Menu -->
    <nav class="mt-2">
      <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
        <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
        <li class="nav-item has-treeview">
          <a href="{{url('admin')}}" class="nav-link ">
            <i class="nav-icon fa fa-tachometer-alt"></i>
            <p>
              Dashboard
              <!-- <i class="right fas fa-angle-left"></i> -->
            </p>
          </a>
          <!-- <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="#" class="nav-link active">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Active Page</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="#" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Inactive Page</p>
                </a>
              </li>
            </ul> -->
        </li>
        <!-- <li class="nav-item">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-th"></i>
              <p>
                Simple Link
                <span class="right badge badge-danger">New</span>
              </p>
            </a>
          </li> -->

        <li class="nav-item has-treeview ">
          <a href="#" class="nav-link ">
            <i class="nav-icon fa fa-globe"></i>
            <p>
              Duallah Utara
              <i class="right fas fa-angle-left"></i>
            </p>
          </a>
          <ul class="nav nav-treeview">
            <!-- <li class="nav-item">
              <a href="/wilayah" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>potensi wilayah</p>
              </a>
            </li> -->
            <li class="nav-item">
              <a href="{{url('wisata')}}" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>potensi wisata</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="/sda" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>potensi sumber daya alam</p>
              </a>
            </li>
          </ul>
        </li>


        <li class="nav-item">
          <a href="{{url('desa')}}" class="nav-link">
            <i class="nav-icon fa fa-location-arrow "></i>
            <p>
              Desa
            </p>
          </a>
        </li>

        <!-- <li class="nav-item">
          <a href="{{url('berita')}}" class="nav-link">
            <i class="nav-icon fa fa-file-text"></i>
            <p>
              Berita
            </p>
          </a>
        </li> -->
        
        <li class="nav-item">
          <a href="/pengurus" class="nav-link">
            <i class="nav-icon fa fa-users "></i>
            <p>
              Pengurus
            </p>
          </a>
        </li>

        <li class="nav-item ">
          <p>
             master
          </p>
          </a>
        </li>

        <!-- <li class="nav-item">
          <a href="#" class="nav-link">
            <i class="nav-icon fa fa-users "></i>
            <p>
              User
            </p>
          </a>
        </li> -->
        <li class="nav-item">
          <a href="{{url('/user')}}" class="nav-link">
            <i class="nav-icon fa fa-user "></i>
            <p>
              Manage Admin
            </p>
          </a>
        </li>

      </ul>
    </nav>
    <!-- /.sidebar-menu -->
  </div>
  <!-- /.sidebar -->
</aside>