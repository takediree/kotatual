<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">

<head>
    @include('template.head')
</head>

<body class="hold-transition sidebar-mini">
    <div class="wrapper">

        <!-- Navbar -->
        @include('template.navbar')
        <!-- /.navbar -->

        <!-- Main Sidebar Container -->
        @include('template.sidebar')
        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <div class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                            <h1 class="m-0 text-dark">Wisata</h1>
                        </div><!-- /.col -->

                    </div><!-- /.row -->
                </div><!-- /.container-fluid -->
            </div>
            <!-- /.content-header -->

            <!-- Main content -->
            <!-- Main content -->
    <div class="container">
            <a href="/wisata/tambah" class="btn btn-primary btn-sm">Tambah data</a>
<br> <br>
            @if (session('status'))

            <div class="alert alert-danger col-md-4">
                {{session('status')}}
            </div>

            @endif

            <table class="table">
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">id_desa</th>
                        <th scope="col">Nama wisata</th>
                        <th scope="col">Longitude</th>
                        <th scope="col">Latitude</th>
                        <th scope="col">Keterangan</th>
                        <th scope="col">Gambar</th>
                        <th scope="col">action</th>

                    </tr>
                </thead>
                <tbody>
                    @foreach ( $wisata as $wis )
                    <tr>
                        <th scope="row">{{$loop->iteration}}</th>
                        <td>{{ $wis->id}}</td>
                        <td>{{ $wis->nama_wisata}}</td>
                        <td>{{ $wis->longitude}}</td>
                        <td>{{ $wis->latitude}}</td>
                        <td>{{ $wis->keterangan}}</td>
                        <td><img src="{{ asset('uploads/'.$wis->gambar) }}" height="75" width="75" alt=""/></td>


                        <td>

                            <a class="btn btn-success btn-xs" href="{{ url("wisata/edit",$wis->id) }}">Edit</a>
                            <a class="btn btn-danger btn-xs" href="{{ url("wisata/delete",$wis->id) }}">Hapus</a>
                    
                        </td>

                    </tr>
                    @endforeach

                </tbody>
            </table>
            <!-- /.content -->
            </div>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->

        <!-- Control Sidebar -->
        <aside class="control-sidebar control-sidebar-dark">
            <!-- Control sidebar content goes here -->
            <div class="p-3">
                <h5>Title</h5>
                <p>Sidebar content</p>
            </div>
        </aside>
        <!-- /.control-sidebar -->

        <!-- Main Footer -->
        <footer class="main-footer">
            @include('template.footer')
        </footer>
    </div>
    <!-- ./wrapper -->

    <!-- REQUIRED SCRIPTS -->

    <!-- jQuery -->
    @include('template.script')
</body>

</html>