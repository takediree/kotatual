<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">

<head>
    @include('template.head')
</head>

<body class="hold-transition sidebar-mini">
    <div class="wrapper">

        <!-- Navbar -->
        @include('template.navbar')
        <!-- /.navbar -->

        <!-- Main Sidebar Container -->
        @include('template.sidebar')
        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <div class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                            <h1 class="m-0 text-dark">Starter Page</h1>
                        </div><!-- /.col -->
                        
                    </div><!-- /.row -->
                </div><!-- /.container-fluid -->
            </div>
            <!-- /.content-header -->

            <!-- Main content -->
            <section class="container">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6">
                <div class="card card-primary  ">
                
             
                <div class="card-header ">
                    <h3 class="card-title ">Tambah Data Wisata</h3>
                </div>
                <!-- /.card-header -->
                <!-- form start -->
                <form method="post" action="/wisata" enctype="multipart/form-data">
                    @csrf
                    <div class="card-body ">
                        <div class="form-group">
                            <label for="nama_wisata">Nama Wisata</label>
                            <input type="text" class="form-control @error('nama_wisata') is-invalid @enderror" id="nama_wisata" placeholder="Enter Nama Wisata" name="nama_wisata" value={{old('nama_wisata')}}>
                            
                        </div>

                        <div class="form-group ">
                            <label for="desa">Desa</label>
                            <select  class="form-control @error('id_desa') is-invalid @enderror" name="id_desa" id="id_desa" value={{old('id_desa')}}>
                                <option selected>Desa</option>
                                @foreach($desa as $d)
                                        <option value={{ $d->id }}>{{ $d->nama_desa }}</option>
                                @endforeach
                            </select>
                        </div>

                        
           

                        <div class="form-group">
                            <label for="longitude">Longitude</label>
                            <input type="text" class="form-control @error('longitude') is-invalid @enderror" id="longitude" placeholder="Enter longitude" name="longitude" value={{old('longitude')}}>
                        </div>
                        <div class="form-group">
                            <label for="latitude">Latitude</label>
                            <input type="text" class="form-control @error('latitude') is-invalid @enderror" id="latitude" placeholder="Enter latitude" name="latitude" value={{old('latitude')}}>
                        </div>

                        <div class="form-group">
                            <label for="keterangan">Keterangan</label>
                            <textarea class="form-control " id="keterangan" placeholder="Enter Keterangan" name="keterangan" rows="4" cols="50" >{{old('keterangan')}}</textarea>
                            
                        </div>

                        <div class="form-group">
                            <label for="gambar">Gambar</label>
                            <div class="input-group">
                                <div class="custom-file">
                                    <input type="file" id="gambar" name="gambar" value={{old('nama_wisata')}}>
                                </div>
                                
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                    <!-- /.card-body -->



                </form>
            </div>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->
            
                
                </div>
            </div>

        </div>

       </section>

        <!-- Control Sidebar -->
        <aside class="control-sidebar control-sidebar-dark">
            <!-- Control sidebar content goes here -->
            <div class="p-3">
                <h5>Title</h5>
                <p>Sidebar content</p>
            </div>
        </aside>
        <!-- /.control-sidebar -->

        <!-- Main Footer -->
        <footer class="main-footer">
            @include('template.footer')
        </footer>
    </div>
    <!-- ./wrapper -->

    <!-- REQUIRED SCRIPTS -->

    <!-- jQuery -->
    @include('template.script')
</body>

</html>