<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">

<head>
    @include('template.head')
</head>

<body class="hold-transition sidebar-mini">
    <div class="wrapper">

        <!-- Navbar -->
        @include('template.navbar')
        <!-- /.navbar -->

        <!-- Main Sidebar Container -->
        @include('template.sidebar')
        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <div class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                            <h1 class="m-0 text-dark">Desa</h1>
                        </div><!-- /.col -->

                    </div><!-- /.row -->
                </div><!-- /.container-fluid -->
            </div>
            <!-- /.content-header -->

            <!-- Main content -->
            <section class="content">
            <a href="/desa/tambah" class="btn btn-primary btn-sm">Tambah data</a>
<br> <br>
                @if (session('status'))

                <div class="alert alert-success col-md-4">
                    {{session('status')}}
                </div>

                @endif

                <table class="table">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Nama Desa</th>
                            <th scope="col">Longitude</th>
                            <th scope="col">Latitude</th>
                            <th scope="col">GeoJSON</th>
                            <th scope="col">Keterangan</th>
                            <th scope="col">gambar</th>
                            <th scope="col">action</th>

                        </tr>
                    </thead>
                    <tbody>
                        @foreach ( $desa as $des )
                        <tr>
                            <th scope="row">{{$loop->iteration}}</th>
                            <td>{{ $des->nama_desa}}</td>
                            <td>{{ $des->longitude}}</td>
                            <td>{{ $des->latitude}}</td>
                            <td>{{ $des->geo_json}}</td>
                            <td>{{ $des->keterangan}}</td>
                            <td><img src="{{ asset('uploads/'.$des->gambar) }}" height="75" width="75" alt=""></td>


                            <td>
                                
                                <form action="{{ url("desa/".$des->id) }}" method="Post"  class="d-inline" >
                                <a class="btn btn-success btn-xs" href="{{ url("desa/edit",$des->id) }}">Edit</a>
                                @csrf
                                @method('delete')
                                    <button type="submit" class="btn btn-danger btn-xs">Delete</button>
                            </form>
                            </td>
 
                        </tr>
                        @endforeach

                    </tbody>
                </table>
                <!-- /.content -->
                <!-- /.content -->
                </div>

            </section>
            <!-- Main content -->
         
        </div>
        <!-- /.content-wrapper -->

        <!-- Control Sidebar -->
        <aside class="control-sidebar control-sidebar-dark">
            <!-- Control sidebar content goes here -->
            <div class="p-3">
                <h5>Title</h5>
                <p>Sidebar content</p>
            </div>
        </aside>
        <!-- /.control-sidebar -->

        <!-- Main Footer -->
        <footer class="main-footer">
            @include('template.footer')
        </footer>
    </div>
    <!-- ./wrapper -->

    <!-- REQUIRED SCRIPTS -->

    <!-- jQuery -->
    @include('template.script')
</body>

</html>