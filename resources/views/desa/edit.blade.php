<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">

<head>
    @include('template.head')
</head>

<body class="hold-transition sidebar-mini">
    <div class="wrapper">

        <!-- Navbar -->
        @include('template.navbar')
        <!-- /.navbar -->

        <!-- Main Sidebar Container -->
        @include('template.sidebar')
        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <div class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                            <h1 class="m-0 text-dark">Edit data</h1>
                        </div><!-- /.col -->
                        <div class="col-sm-6">
                            <ol class="breadcrumb float-sm-right">
                                <li class="breadcrumb-item"><a href="#">Home</a></li>
                                <li class="breadcrumb-item active">Edit data</li>
                            </ol>
                        </div><!-- /.col -->
                    </div><!-- /.row -->
                </div><!-- /.container-fluid -->
            </div>
            <!-- /.content-header -->

            <!-- Main content -->
            <section class="container">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6">
                <div class="card card-primary  ">
                <div class="card card-primary ">
                <div class="card-header ">
                    <h3 class="card-title ">Edit Data Desa</h3>
                </div>
                <!-- /.card-header -->
                <!-- form start -->
                <form method="post" action="{{ url("desa/".$desa->id) }}"  enctype="multipart/form-data">
                    @csrf
                    @method('PUT')
                    <div class="card-body ">
                        <div class="form-group">
                            <label for="nama_desa">Nama Desa</label>
                            <input type="text" class="form-control @error('nama_desa') is-invalid @enderror" id="nama_desa" placeholder="Enter Nama Desa" name="nama_desa" value="{{$desa->nama_desa}}">
                        </div>
                 

                        <div class="form-group">
                            <label for="longitude">Longitude</label>
                            <input type="text" class="form-control @error('longitude') is-invalid @enderror" id="longitude" placeholder="Enter longitude" name="longitude" value="{{$desa->longitude}}">
                        </div>
                        <div class="form-group">
                            <label for="latitude">Latitude</label>
                            <input type="text" class="form-control @error('latitude') is-invalid @enderror" id="latitude" placeholder="Enter latitude" name="latitude" value="{{$desa->latitude}}">
                        </div>

                        <div class="form-group">
                            <label for="keterangan">Keterangan</label>
                            <textarea class="form-control @error('keterangan') is-invalid @enderror" id="keterangan" placeholder="Enter Keterangan" name="keterangan" rows="4" cols="50">{{$desa->keterangan}}</textarea>
                            
                        </div>
                        <div class="form-group">
                            <label for="geo_json">GeoJSON</label>
                            <div class="input-group">
                                <div class="custom-file">
                                    <input type="file" id="geo_json" name="geo_json">
                                </div>
                                
                            </div>

                        <div class="form-group">
                            <label for="gambar">Gambar</label>
                            <div class="input-group">
                                <div class="custom-file">
                                    <input type="file" id="gambar" name="gambar" >
                                </div>
                                
                            </div>
                        </div>


                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                    <!-- /.card-body -->



                </form>
            </div>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->
            
                
                </div>
            </div>

        </div>

       </section>

        <!-- Control Sidebar -->
        <aside class="control-sidebar control-sidebar-dark">
            <!-- Control sidebar content goes here -->
            <div class="p-3">
                <h5>Title</h5>
                <p>Sidebar content</p>
            </div>
        </aside>
        <!-- /.control-sidebar -->

        <!-- Main Footer -->
        <footer class="main-footer">
            @include('template.footer')
        </footer>
    </div>
    <!-- ./wrapper -->

    <!-- REQUIRED SCRIPTS -->

    <!-- jQuery -->
    @include('template.script')
</body>

</html>