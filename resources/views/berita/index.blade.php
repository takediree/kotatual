<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">

<head>
    @include('template.head')
</head>

<body class="hold-transition sidebar-mini">
    <div class="wrapper">

        <!-- Navbar -->
        @include('template.navbar')
        <!-- /.navbar -->

        <!-- Main Sidebar Container -->
        @include('template.sidebar')
        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <div class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                            <h1 class="m-0 text-dark">Berita</h1>
                        </div><!-- /.col -->

                    </div><!-- /.row -->
                </div><!-- /.container-fluid -->
            </div>
            <!-- /.content-header -->

            <!-- Main content -->
            <!-- Main content -->
        <div class="container">    
            <a href="/berita/tambah" class="btn btn-primary btn-sm " >Tambah data</a>

            @if (session('status'))

            <div class="alert alert-success col-md-4">
                {{session('status')}}
            </div>

            @endif

            <table class="table" >
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Tanggal</th>
                        <th scope="col">Judul</th>
                        <th scope="col">Isi</th>
                        <th scope="col">Gambar</th>
                        <th scope="col">Action</th>

                    </tr>
                </thead>
                <tbody>
                    @foreach ( $berita as $beritaa )
                    <tr>
                        <th scope="row">{{$loop->iteration}}</th>
                        <td>{{ $beritaa->tanggal}}</td>
                        <td>{{ $beritaa->judul}}</td>
                        <td>{{ $beritaa->isi}}</td>
                        <td><img src="{{ Storage::url($beritaa->gambar) }}" height="75" width="75" alt=""/></td>
                       


                        <td>
                            <!-- <button type="button" class="btn btn-success btn-xs"> Edit</button>
                        <a class="btn btn-primary" href="edit/{{$beritaa->id}}">Edit</a> -->

                        <form action="{{ url("berita/".$beritaa->id) }}" method="Post"  class="d-inline" >
                            <a class="btn btn-primary btn-xs" href="{{ url("berita/edit",$beritaa->id) }}">Edit</a>
                               @csrf
                               @method('delete')
                                <button type="submit" class="btn btn-danger btn-xs">Delete</button>
                        </form>
                            <!-- <form action="{{ url("berita/".$beritaa->id) }}" method="POST" class="d-inline">
                                @csrf
                                @method('delete')
                                <button type="submit" class="btn btn-danger btn-xs"> hapus</button>
                            </form> -->
                        </td>

                    </tr>
                    @endforeach

                </tbody>
            </table>
            <!-- /.content -->
            <!-- /.content -->
            </div>
        </div>
        <!-- /.content-wrapper -->

        <!-- Control Sidebar -->
        <aside class="control-sidebar control-sidebar-dark">
            <!-- Control sidebar content goes here -->
            <div class="p-3">
                <h5>Title</h5>
                <p>Sidebar content</p>
            </div>
        </aside>
        <!-- /.control-sidebar -->

        <!-- Main Footer -->
        <footer class="main-footer">
            @include('template.footer')
        </footer>
    </div>
    <!-- ./wrapper -->

    <!-- REQUIRED SCRIPTS -->

    <!-- jQuery -->
    @include('template.script')
</body>

</html>