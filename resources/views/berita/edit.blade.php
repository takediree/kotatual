<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">

<head>
    @include('template.head')
</head>

<body class="hold-transition sidebar-mini">
    <div class="wrapper">

        <!-- Navbar -->
        @include('template.navbar')
        <!-- /.navbar -->

        <!-- Main Sidebar Container -->
        @include('template.sidebar')
        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <div class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                            <h1 class="m-0 text-dark">Edit</h1>
                        </div><!-- /.col -->
                        <div class="col-sm-6">
                            <ol class="breadcrumb float-sm-right">
                                <li class="breadcrumb-item"><a href="#">Home</a></li>
                                <li class="breadcrumb-item active">Edit</li>
                            </ol>
                        </div><!-- /.col -->
                    </div><!-- /.row -->
                </div><!-- /.container-fluid -->
            </div>
            <!-- /.content-header -->

            <!-- Main content -->
            <section class="container">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6">
                <div class="card card-primary  ">
                <div class="card-header ">
                    <h3 class="card-title ">Edit Data Berita</h3>
                </div>
                <!-- /.card-header -->
                <!-- form start -->
                <form method="post" action="{{ url("berita/".$berita->id) }}" enctype="multipart/form-data">
                    @csrf
                @method('PUT')
                    <div class="card-body ">
                        <div class="form-group">
                            <label for="tangggal">Tanggal</label>
                            <input type="text" class="form-control" id="tanggal" placeholder="Enter tanggal" name="tanggal" value="{{$berita->tanggal}}">
                        </div>
                        <!-- <div class="form-group ">
              <label for="exampleFormControlSelect1">Tipe Bengkel</label>
              <select class="form-control" id="exampleFormControlSelect1">
                <option>Toyota</option>
                <option>Daihatsu</option>
                <option>Honda</option>
                <option>Mitsubishi</option>
                <option>ford</option>
              </select>
            </div> -->

                        <div class="form-group">
                            <label for="Judul">Judul</label>
                            <input type="text" class="form-control" id="Judul" placeholder="Enter Judul" name="judul" value="{{$berita->judul}}">
                        </div>

                        <div class="form-group">
                            <label for="isi">Isi</label>
                            <textarea class="form-control" id="isi" placeholder="Enter isi" name="isi" >{{$berita->isi}}</textarea>
                        </div>

                        <div class="form-group">
                            <label for="gambar">Gambar</label>

                            <div class="input-group">
                                <div class="custom-file">
                                    <input type="file" id="gambar" name="gambar"  value="{{$berita->gambar}}">
                                </div>

                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>

                    <!-- /.card-body -->
                </form>
            </div>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->
            
                
                </div>
            </div>

        </div>

       </section>

        <!-- Control Sidebar -->
        <aside class="control-sidebar control-sidebar-dark">
            <!-- Control sidebar content goes here -->
            <div class="p-3">
                <h5>Title</h5>
                <p>Sidebar content</p>
            </div>
        </aside>
        <!-- /.control-sidebar -->

        <!-- Main Footer -->
        <footer class="main-footer">
            @include('template.footer')
        </footer>
    </div>
    <!-- ./wrapper -->

    <!-- REQUIRED SCRIPTS -->

    <!-- jQuery -->
    @include('template.script')
</body>

</html>