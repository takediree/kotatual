<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="zxx">
<!--<![endif]-->

<!-- Mirrored from demos.jeweltheme.com/cast/index-02.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 30 Jul 2019 21:11:47 GMT -->

<head>
   
    @include('user1.template.head')

</head>


<body>



    <header class="header">
        <div class="header-top gray-bg">
           @include('user1.template.header')
        </div><!-- /.header-top -->

        <div class="header-bottom">
            @include('user1.template.navbar')
        </div><!-- /.header-bottom -->
    </header><!-- /.header -->




    <section class="">
        <div class="section-padding">
            <div class="container">

                <div  class="banner-slider carousel slide text-left">
                    <div >

                        <div class="item item-2 active">
                            <img src="{{asset('cast/images/main_banner.jpg')}}" alt="">
                            <div class="inner-contents">
                                <div class="inner-texts">
                                    <h2 class="item-title " style="color:white;">Selamat Datang</h2><!-- /.item-title -->
                                    <p class="description" style="color:#999999;">
                                        Ini Adalah Layanan Sistem Informasi Geografis Kecamatan Dullah Utara Kota Tual
                                    </p><!-- /.description -->

                                    <div class="btn-container">
                                        
                                        <a href="http://127.0.0.1:8000/user1/map" class="btn black">Location</a>
                                    </div><!-- /.btn-container -->
                                </div><!-- /.inner-texts -->

                               
                            </div><!-- /.inner-contents -->
                        </div><!-- /.item -->

                        
                    </div><!-- /.carousel-inner -->

                   
                </div><!-- /#banner-slider -->

            </div><!-- /.container -->
        </div><!-- /.section-padding -->
    </section><!-- /.slider-section -->
 
    <section class="welcome welcome-2">
        <div class="section-padding">
            <div class="container">
                <div class="items">
                    <div class="col-sm-8">
                        <div class="inner-bg">
                            <h2 class="section-title">Gis Kecamatan Dullah Utara</h2><!-- /.section-title -->
                            <div class="padding">
                                <p class="description">
                                    ini adalah layanan sisitem informasi Geografis Kecamatan Dullah Utara Kota Tual Provinsi Maluku Indonesi 
                                    yang mencakup tentang potensi wilayah, potensi wisata, potensi sumber daya alam yang terdapat di wilayah Kecamatan Dullah Utara.
                                    <br>
                                    <br>
                                   
                                </p><!-- /.description -->
                                <!-- <a href="user1/about" class="btn read-more">Read more<i class="ti-arrow-right"></i></a> -->
                            </div><!-- /.padding -->

                            <!-- <img src="images/1.png" alt="Image"> -->
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="inner-bg">
                            <div class="item">
                                <div class="item-icon"><i class="ti-user"></i></div><!-- /.item-icon -->
                                <div class="item-details">
                                    <h3 class="item-title">Penentuan Lokasi</h3><!-- /.item-title -->
                                    <p class="description">
                                        Penentuan lokasi desa dan potensi di desa tersebut
                                    </p><!-- /.description -->
                                </div><!-- /.item-details -->
                            </div><!-- /.item -->

                           

                            <div class="item">
                                <div class="item-icon"><i class="ti-calendar"></i></div><!-- /.item-icon -->
                                <div class="item-details">
                                    <h3 class="item-title">Updete</h3><!-- /.item-title -->
                                    <p class="description">
                                       Memberikan Informasi yang updete
                                    </p><!-- /.description -->
                                </div><!-- /.item-details -->
                            </div><!-- /.item -->
                        </div><!-- /.inner-bg -->
                    </div>
                </div><!-- /.items -->
            </div><!-- /.container -->
        </div><!-- /.section-padding -->
    </section><!-- /.welcome -->




    <section class="team-section">
        <div class="section-padding">
            <div class="container">
                <div class="inner-bg">
                    <h2 class="section-title">Our Team</h2>
                    <div class="padding">
                        
                        <div class="items">
                            <div id="team-slider" class="team-slider owl-carousel owl-theme text-center">
                                @foreach($pengurus as $p)
                                <div class="item">
                                    <div class="member">
                                        <div class="member-image"><img src="{{ asset('uploads/'.$p->gambar) }}" alt="Member Image"></div><!-- /.member-image -->
                                        <div class="member-details">
                                            <h3 class="name item-title"><a href="#">{{$p->nama}}</a></h3><!-- /.name -->
                                            <span class="designation">{{ $p->jabatan }}</span><!-- /.designation -->
                                            <div class="member-social">
                                                <a href="#"><i class="fa fa-facebook"></i></a>
                                                <a href="#"><i class="fa fa-twitter"></i></a>
                                                <a href="#"><i class="fa fa-google-plus-circle"></i></a>
                                            </div><!-- /.member-social -->
                                        </div><!-- /.member-details -->
                                    </div><!-- /.member -->
                                </div><!-- /.item -->
                                @endforeach
                                
                            </div><!-- /#team-slider -->
                        </div><!-- /.items -->
                    </div><!-- /.padding -->
                </div><!-- /.inner-bg -->
            </div><!-- /.container -->
        </div><!-- /.section-padding -->
    </section><!-- /.team-section -->




  




    <footer class="site-footer">
        @include('user1.template.footer')
    </footer><!-- /.footer-bottom -->


    <script src="{{asset('cast/assets/js/plugins.js')}}"></script>
    <script src="{{asset('cast/assets/js/main.js')}}"></script>


</body>

<!-- Mirrored from demos.jeweltheme.com/cast/index-02.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 30 Jul 2019 21:11:50 GMT -->

</html>