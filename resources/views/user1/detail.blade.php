<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="zxx"> <!--<![endif]-->

<!-- Mirrored from demos.jeweltheme.com/cast/blog-single.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 30 Jul 2019 21:12:28 GMT -->
<head>
 
    @include('user1.template.head')

</head>


<body>

   <header class="header">
        <div class="header-top gray-bg">
           @include('user1.template.header')
        </div><!-- /.header-top -->

        <div class="header-bottom">
            @include('user1.template.navbar')
        </div><!-- /.header-bottom -->
    </header><!-- /.header -->



    <section class="page-name">
        <div class="section-padding">
            <div class="container">
                <div class="name-section background-bg" data-image-src="images/bg7.jpg">
                    <div class="overlay">
                        <div class="padding">
                            <h2 class="page-title">Company blog</h2><!-- /.page-ttile -->
                            <ol class="breadcrumb">
                                <li><a href="#">Home</a></li>
                                <li><a href="#">Blog</a></li>
                                <li class="active">Single Post</li>
                            </ol>
                        </div><!-- /.padding -->
                    </div><!-- /.overlay -->
                </div><!-- /.name-section -->
            </div><!-- /.container -->
        </div><!-- /.section-padding -->
    </section><!-- /.page-name -->




    <section class="blog-posts single-layout">
        <div class="section-padding">
            <div class="container">
                <div class="items">
                    <div class="col-sm-8">
                        <article class="post type-post">
                            <div class="entry-thumbnail"><img src="images/post/single.jpg" alt="Entry Thumbnail"></div><!-- /.entry-thumbnail -->

                            <div class="entry-content">
                                <h3 class="entry-title"></h3><!-- /.entry-title -->
                                <div class="entry-meta">
                                    <span><time datetime="2016-11-15">Nov 15, 2016</time></span>
                                    <span>Posted by <a href="#">Anthony</a></span>
                                    <span>with <a href="#">17 Comments</a></span>
                                </div><!-- /.entry-meta -->

                                <div class="description">
                                    <p>
                                        <strong>
                                            Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium 
                                        </strong>
                                        <br><br>
                                        Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.
                                    </p>
                                    <blockquote>
                                        “Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.”
                                        <span class="name">Source Name</span>
                                    </blockquote>
                                    <p>
                                        Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.  
                                    </p>
                                </div><!-- /.description -->

                                <!-- <div class="content-bottom">
                                    <span class="meta-id"><i class="fa fa-bookmark"></i> Category: <span><a href="#">architecture</a></span></span>
                                    <span class="meta-id"><i class="fa fa-tags"></i>Tags: <span><a href="#">Design</a><a href="#">Renovation</a><a href="#">Builder</a></span></span>
                                </div> -->
                            </div><!-- /.entry-content -->

                            
                        </article><!-- /.post -->

                    </div>
                    
                 <!-- sidebar -->
                    <div class="col-sm-4">
                        @include('user1.template.sidebar')
                       
                    </div>
                </div><!-- /.items -->
            </div><!-- /.container -->
        </div><!-- /.section-padding -->
    </section><!-- /.blog-posts -->



    <section class="related-post">
        <div class="section-padding">
            <div class="container">
                <div class="inner-bg">
                    <h2 class="section-title">Related News</h2><!-- /.section-title -->

                    <div class="padding">
                        <div class="items">
                            <div id="related-post-slider" class="related-post-slider owl-carousel owl-theme">
                                <div class="item">
                                    <article class="post type-post">
                                        <div class="entry-thumbnail"><img src="images/post/14.jpg" alt="Entry Thumbnail"></div><!-- /.entry-thumbnail -->
                                        <div class="entry-content">
                                            <h3 class="entry-title"><a href="single.html">Marla Kraft earns real estate designation</a></h3><!-- /.entry-title -->
                                            <div class="entry-meta">
                                                <span class="time"><time datetime="2016-11-28">Nov 28, 2016</time></span>
                                                <span class="author">by <a href="#">Anthony</a></span>
                                            </div><!-- /.entry-meta -->
                                            <p class="description">
                                                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud
                                            </p><!-- /.description -->
                                            <a href="single.html" class="btn read-more">Read more</a><!-- /.btn -->
                                        </div><!-- /.entry-content -->
                                    </article><!-- /.post.type-post -->
                                </div><!-- /.item -->

                                <div class="item">
                                    <article class="post type-post">
                                        <div class="entry-thumbnail"><img src="images/post/15.jpg" alt="Entry Thumbnail"></div><!-- /.entry-thumbnail -->
                                        <div class="entry-content">
                                            <h3 class="entry-title"><a href="single.html">Greenland HK raises $300 mln for real estate fund with Kuwait</a></h3><!-- /.entry-title -->
                                            <div class="entry-meta">
                                                <span class="time"><time datetime="2016-11-28">Nov 28, 2016</time></span>
                                                <span class="author">by <a href="#">Anthony</a></span>
                                            </div><!-- /.entry-meta -->
                                            <p class="description">
                                                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud
                                            </p><!-- /.description -->
                                            <a href="single.html" class="btn read-more">Read more</a><!-- /.btn -->
                                        </div><!-- /.entry-content -->
                                    </article><!-- /.post.type-post -->
                                </div><!-- /.item -->

                                <div class="item">
                                    <article class="post type-post">
                                        <div class="entry-thumbnail"><img src="images/post/16.jpg" alt="Entry Thumbnail"></div><!-- /.entry-thumbnail -->
                                        <div class="entry-content">
                                            <h3 class="entry-title"><a href="single.html">Trump talks Treasury with Blackstone real estate boss</a></h3><!-- /.entry-title -->
                                            <div class="entry-meta">
                                                <span class="time"><time datetime="2016-11-28">Nov 28, 2016</time></span>
                                                <span class="author">by <a href="#">Anthony</a></span>
                                            </div><!-- /.entry-meta -->
                                            <p class="description">
                                                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud
                                            </p><!-- /.description -->
                                            <a href="single.html" class="btn read-more">Read more</a><!-- /.btn -->
                                        </div><!-- /.entry-content -->
                                    </article><!-- /.post.type-post -->
                                </div><!-- /.item -->

                                <div class="item">
                                    <article class="post type-post">
                                        <div class="entry-thumbnail"><img src="images/post/4.jpg" alt="Entry Thumbnail"></div><!-- /.entry-thumbnail -->
                                        <div class="entry-content">
                                            <h3 class="entry-title"><a href="single.html">Marla Kraft earns real estate designation</a></h3><!-- /.entry-title -->
                                            <div class="entry-meta">
                                                <span class="time"><time datetime="2016-11-28">Nov 28, 2016</time></span>
                                                <span class="author">by <a href="#">Anthony</a></span>
                                            </div><!-- /.entry-meta -->
                                            <p class="description">
                                                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud
                                            </p><!-- /.description -->
                                            <a href="single.html" class="btn read-more">Read more</a><!-- /.btn -->
                                        </div><!-- /.entry-content -->
                                    </article><!-- /.post.type-post -->
                                </div><!-- /.item -->

                                <div class="item">
                                    <article class="post type-post">
                                        <div class="entry-thumbnail"><img src="images/post/5.jpg" alt="Entry Thumbnail"></div><!-- /.entry-thumbnail -->
                                        <div class="entry-content">
                                            <h3 class="entry-title"><a href="single.html">Greenland HK raises $300 mln for real estate fund with Kuwait</a></h3><!-- /.entry-title -->
                                            <div class="entry-meta">
                                                <span class="time"><time datetime="2016-11-28">Nov 28, 2016</time></span>
                                                <span class="author">by <a href="#">Anthony</a></span>
                                            </div><!-- /.entry-meta -->
                                            <p class="description">
                                                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud
                                            </p><!-- /.description -->
                                            <a href="single.html" class="btn read-more">Read more</a><!-- /.btn -->
                                        </div><!-- /.entry-content -->
                                    </article><!-- /.post.type-post -->
                                </div><!-- /.item -->

                                <div class="item">
                                    <article class="post type-post">
                                        <div class="entry-thumbnail"><img src="images/post/6.jpg" alt="Entry Thumbnail"></div><!-- /.entry-thumbnail -->
                                        <div class="entry-content">
                                            <h3 class="entry-title"><a href="single.html">Trump talks Treasury with Blackstone real estate boss</a></h3><!-- /.entry-title -->
                                            <div class="entry-meta">
                                                <span class="time"><time datetime="2016-11-28">Nov 28, 2016</time></span>
                                                <span class="author">by <a href="#">Anthony</a></span>
                                            </div><!-- /.entry-meta -->
                                            <p class="description">
                                                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud
                                            </p><!-- /.description -->
                                            <a href="single.html" class="btn read-more">Read more</a><!-- /.btn -->
                                        </div><!-- /.entry-content -->
                                    </article><!-- /.post.type-post -->
                                </div><!-- /.item -->
                            </div><!-- /#latest-post-slider -->
                        </div><!-- /.items -->
                    </div><!-- /.padding -->
                </div><!-- /.inner-bg -->
            </div><!-- /.container -->
        </div><!-- /.section-padding -->
    </section><!-- /.related-post -->



    <footer class="site-footer">
        @include('user1.template.footer')
    </footer><!-- /.footer-bottom -->


    <script src="{{asset('cast/assets/js/plugins.js')}}"></script>
    <script src="{{asset('cast/assets/js/main.js')}}"></script>


</body>

<!-- Mirrored from demos.jeweltheme.com/cast/blog-single.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 30 Jul 2019 21:12:32 GMT -->
</html>
