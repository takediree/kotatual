<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="zxx"> <!--<![endif]-->

<!-- Mirrored from demos.jeweltheme.com/cast/contact.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 30 Jul 2019 21:12:47 GMT -->
<head>
  
    @include('user1.template.head')
    

</head>


<body>



    <header class="header">
        <div class="header-top gray-bg">
           @include('user1.template.header')
        </div><!-- /.header-top -->

        <div class="header-bottom">
            @include('user1.template.navbar')
        </div><!-- /.header-bottom -->
    </header><!-- /.header -->




    <section class="page-name">
        <div class="section-padding">
            <div class="container">
                <div class="name-section background-bg" data-image-src="images/bg.jpg">
                    <div class="overlay">
                        <div class="padding">
                            <h2 class="page-title">Map</h2><!-- /.page-ttile -->
                            <ol class="breadcrumb">
                                <li><a href="#">Home</a></li>
                                <li class="active">Map</li>
                            </ol>
                        </div><!-- /.padding -->
                    </div><!-- /.overlay -->
                </div><!-- /.name-section -->
            </div><!-- /.container -->
        </div><!-- /.section-padding -->
    </section><!-- /.page-name -->



    <section id="google-map">
        <div class="section-padding">
            <div class="container">
                <div class="map-container">
<!--                    <div id="googleMaps" class="google-map-container"></div> -->

                        <div id="google-map" style="width:100%; height:400px;"></div>

                </div><!-- /.map-container -->
            </div><!-- /.container -->
        </div><!-- /.section-padding -->
    </section><!-- /#google-map-->


    <footer class="site-footer">
        @include('user1.template.footer')
    </footer><!-- /.footer-bottom -->


    <script src="{{asset('cast/assets/js/plugins.js')}}"></script>
    <script src="{{asset('cast/assets/js/main.js')}}"></script>
  
    
     <!-- Make sure you put this AFTER Leaflet's CSS -->
    <script src="https://unpkg.com/leaflet@1.7.1/dist/leaflet.js"
    integrity="sha512-XQoYMqMTK8LvdxXYG3nZ448hOEQiglfqkJs1NOQV44cWnUrBc8PkAOcXy20w0vlaXaVUearIOBhiXZ5V3ynxwA=="
    crossorigin=""></script>

    <script src="{{asset('js/leaflet.ajax.js')}}"></script>
    <script>
    
   
            var mymap = L.map('google-map').setView([-5.611472,132.781396], 12);
            L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
                attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
                maxZoom: 18,
                id: 'mapbox/streets-v11',
                tileSize: 512,
                zoomOffset: -1,
                accessToken: 'pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw'
            }).addTo(mymap);

            var myStyle = {
                    "color": "#ff7800",
                    "weight": 3,
                    "opacity": 0.30
                };
            var myStyle1 = {
                    "color": "#1da093",
                    "weight": 3,
                    "opacity": 0.30
                };
            var myStyle2 = {
                    "color": "#ff7800",
                    "weight": 3,
                    "opacity": 0.30
                };
            var myStyle3 = {
                    "color": "#ff7800",
                    "weight": 3,
                    "opacity": 0.30
                };
            var myStyle4 = {
                    "color": "#ff7800",
                    "weight": 3,
                    "opacity": 0.30
                };
            var myStyle5 = {
                    "color": "#ff7800",
                    "weight": 3,
                    "opacity": 0.30
                };
            var myStyle6 = {
                    "color": "#ff7800",
                    "weight": 3,
                    "opacity": 0.30
                };
            var myStyle7 = {
                    "color": "#ff7800",
                    "weight": 3,
                    "opacity": 0.30
                };

            function popUp(f,l){
                    var out = [];
                    if (f.properties){
                        for(key in f.properties){
                            out.push(key+": "+f.properties[key]);
                                }
                            l.bindPopup(out.join("<br />"));
                            }
                        }
            
               
                   var jsonTest = new L.GeoJSON.AJAX(["{{asset('js/tamedan.geojson')}}"],{onEachFeature:popUp , style: myStyle}).addTo(mymap);
                   var jsonTest = new L.GeoJSON.AJAX(["{{asset('js/labetawi.geojson')}}"],{onEachFeature:popUp , style: myStyle1}).addTo(mymap);
                   var jsonTest = new L.GeoJSON.AJAX(["{{asset('js/dullah.geojson')}}"],{onEachFeature:popUp , style: myStyle2}).addTo(mymap);
                   var jsonTest = new L.GeoJSON.AJAX(["{{asset('js/dullahlaut.geojson')}}" ],{onEachFeature:popUp , style: myStyle3}).addTo(mymap);
                   var jsonTest = new L.GeoJSON.AJAX(["{{asset('js/fiditan.geojson')}}" ],{onEachFeature:popUp , style: myStyle4}).addTo(mymap);
                   var jsonTest = new L.GeoJSON.AJAX(["{{asset('js/ngadi.geojson')}}" ],{onEachFeature:popUp , style: myStyle5}).addTo(mymap);
                   var jsonTest = new L.GeoJSON.AJAX(["{{asset('js/ohitel.geojson')}}" ],{onEachFeature:popUp , style: myStyle6}).addTo(mymap);
                   var jsonTest = new L.GeoJSON.AJAX(["{{asset('js/ohitheit.geojson')}}" ],{onEachFeature:popUp , style: myStyle7}).addTo(mymap);

                   var marker = L.marker([-5.611472, 132.781396]);
                   marker.addTo(mymap);
            
    </script>




</body>

<!-- Mirrored from demos.jeweltheme.com/cast/contact.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 30 Jul 2019 21:12:47 GMT -->
</html>
