<meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>WebGis </title>
  <meta name="description" content="Cast - A Construction & Business HTML5 Template">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="apple-touch-icon" href="apple-touch-icon.png">

  

   

  <!-- ========= FontAwesome Icon Css File ========= -->
  <link rel="stylesheet" href="{{asset('cast/assets/css/themify-icons.css')}}">

  <!-- ========= Themify Icon Css File ========= -->
  <link rel="stylesheet" href="{{asset('cast/assets/css/font-awesome.min.css')}}">

  <!-- ========= Bootstrap Css File ========= -->
  <link rel="stylesheet" href="{{asset('cast/assets/css/bootstrap.min.css')}}">

  <!-- ========= Magnific PopUp Css File ========= -->
  <link rel="stylesheet" href="{{asset('cast/assets/css/magnific-popup.css')}}">

  <!-- ========= Owl Carousel Css File ========= -->
  <link rel="stylesheet" href="{{asset('cast/assets/css/owl.carousel.css')}}"> 

  <!-- ========= Template Default Less File ========= -->
  <link rel="stylesheet" href="{{asset('cast/assets/css/style.css')}}">
  
  <!-- ========= Template Main Style Less File ========= -->
  <link rel="stylesheet" href="{{asset('cast/assets/css/themes.css')}}">

  <!-- ========= Template Demo Style Less File ========= -->
  <link rel="stylesheet" href="{{asset('cast/demo/demo.css')}}"> 

  <!-- ========= Template Responsive Style Less File ========= -->
  <link rel="stylesheet" href="{{asset('cast/assets/css/responsive.css')}}">
  <link rel="stylesheet" href="https://unpkg.com/leaflet@1.7.1/dist/leaflet.css"
   integrity="sha512-xodZBNTC5n17Xt2atTPuE1HxjVMSvLVW9ocqUKLsCC5CXdbqCmblAshOMAS6/keqq/sMZMZ19scR4PsZChSR7A=="
   crossorigin=""/>

  <script src="{{asset('cast/assets/js/modernizr.custom.js')}}"></script>



  <!-- <script type="text/javascript" src="https://cdn.jsdelivr.net/html5shiv/3.7.3/html5shiv.min.js"></script> -->