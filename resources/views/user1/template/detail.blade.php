<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="zxx"> <!--<![endif]-->

<!-- Mirrored from demos.jeweltheme.com/cast/blog-single.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 30 Jul 2019 21:12:28 GMT -->
<head>
   
    @include('user1.template.head')

</head>


<body>

    <header class="header">
        <div class="header-top gray-bg">
           @include('user1.template.header')
        </div><!-- /.header-top -->

        <div class="header-bottom">
            @include('user1.template.navbar')
        </div><!-- /.header-bottom -->
    </header><!-- /.header -->




    <section class="page-name">
        <div class="section-padding">
            <div class="container">
                <div class="name-section background-bg" data-image-src="images/bg7.jpg">
                    <div class="overlay">
                        <div class="padding">
                            <h2 class="page-title">Company blog</h2><!-- /.page-ttile -->
                            <ol class="breadcrumb">
                                <li><a href="#">Home</a></li>
                                <li><a href="#">Blog</a></li>
                                <li class="active">Single Post</li>
                            </ol>
                        </div><!-- /.padding -->
                    </div><!-- /.overlay -->
                </div><!-- /.name-section -->
            </div><!-- /.container -->
        </div><!-- /.section-padding -->
    </section><!-- /.page-name -->




    <section class="blog-posts single-layout">
        <div class="section-padding">
            <div class="container">
                <div class="items">
                    <div class="col-sm-8">
                        <article class="post type-post">
                            <div class="entry-thumbnail"><img src="images/post/single.jpg" alt="Entry Thumbnail"></div><!-- /.entry-thumbnail -->

                            <div class="entry-content">
                                <h3 class="entry-title">North London Victorian Gets Modern Addition</h3><!-- /.entry-title -->
                                <div class="entry-meta">
                                    <span><time datetime="2016-11-15">Nov 15, 2016</time></span>
                                    <span>Posted by <a href="#">Anthony</a></span>
                                    <span>with <a href="#">17 Comments</a></span>
                                </div><!-- /.entry-meta -->

                                <div class="description">
                                    <p>
                                        <strong>
                                            Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium 
                                        </strong>
                                        <br><br>
                                        Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.
                                    </p>
                                    <blockquote>
                                        “Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.”
                                        <span class="name">Source Name</span>
                                    </blockquote>
                                    <p>
                                        Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.  
                                    </p>
                                </div><!-- /.description -->

                                <div class="content-bottom">
                                    <span class="meta-id"><i class="fa fa-bookmark"></i> Category: <span><a href="#">architecture</a></span></span>
                                    <span class="meta-id"><i class="fa fa-tags"></i>Tags: <span><a href="#">Design</a><a href="#">Renovation</a><a href="#">Builder</a></span></span>
                                </div><!-- /.content-bottom -->
                            </div><!-- /.entry-content -->

                          
                        </article><!-- /.post -->
                        
                        


                        

                    </div>

                    <div class="col-sm-4">
                        <aside class="sidebar">
                            <div class="widget widget_search">
                                <div class="widget-details">
                                    <form method="get" class="search-form" action="#">
                                        <input type="text" class="form-control" placeholder="Search" name="search" title="Search.." required="">
                                        <input type="submit" class="form-control" name="submit">
                                    </form>
                                </div><!-- /.widget-details -->
                            </div><!-- /.widget -->

                            <div class="widget widget_categories">
                                <h3 class="widget-title">Categories</h3><!-- /.widget-title -->
                                <div class="widget-details">
                                    <a href="#">WordPress Themes</a>
                                    <a href="#">PSD Template</a>
                                    <a href="#">Construction</a>
                                    <a href="#">Buildings</a>
                                    <a href="#">Green Gaurden</a>
                                    <a href="#">Renovation</a>
                                    <a href="#">Electricity</a>
                                    <a href="#">Painting</a>
                                </div><!-- /.widget-details -->
                            </div><!-- /.widget -->

                            <div class="widget widget_archives">
                                <h3 class="widget-title">Archives</h3><!-- /.widget-title -->
                                <div class="widget-details">
                                    <a href="#">January 2016</a>
                                    <a href="#">February 2016</a>
                                    <a href="#">March 2016</a>
                                    <a href="#">April 2016</a>
                                    <a href="#">May 2016</a>
                                    <a href="#">June 2016</a>
                                    <a href="#">July 2016</a>
                                </div><!-- /.widget-details -->
                            </div><!-- /.widget -->

                            <div class="widget widget_popular_posts">
                                <h3 class="widget-title">Popular Posts</h3><!-- /.widget-title -->
                                <div class="widget-details">
                                    <article class="post type-post media">
                                        <div class="entry-thumbnail media-left"><img src="images/post/11.jpg" alt="Entry Thumbnial"></div><!-- /.entry-thumbnail -->
                                        <div class="entry-content media-body">
                                            <h3 class="entry-title"><a href="blog-single.html">Beautiful Ceiling of Pink Balloons in a French Hotel</a></h3><!-- /.entry-title -->
                                        </div><!-- /.entry-content -->
                                    </article><!-- /.post -->
                                    <article class="post type-post media">
                                        <div class="entry-thumbnail media-left"><img src="images/post/12.jpg" alt="Entry Thumbnial"></div><!-- /.entry-thumbnail -->
                                        <div class="entry-content media-body">
                                            <h3 class="entry-title"><a href="blog-single.html">Hypnotizing Geometry Displayed by Wooden Residence in Estonia</a></h3><!-- /.entry-title -->
                                        </div><!-- /.entry-content -->
                                    </article><!-- /.post -->
                                    <article class="post type-post media">
                                        <div class="entry-thumbnail media-left"><img src="images/post/13.jpg" alt="Entry Thumbnial"></div><!-- /.entry-thumbnail -->
                                        <div class="entry-content media-body">
                                            <h3 class="entry-title"><a href="blog-single.html">Gleaming Bookshelf House in Paris Stores Creativity</a></h3><!-- /.entry-title -->
                                        </div><!-- /.entry-content -->
                                    </article><!-- /.post -->
                                </div><!-- /.widget-details -->
                            </div><!-- /.widget -->

                            <div class="widget widget_tags">
                                <h3 class="widget-title">Tags</h3><!-- /.widget-title -->
                                <div class="widget-details">
                                    <a href="#" class="tag-link">News</a>
                                    <a href="#" class="tag-link">Template</a>
                                    <a href="#" class="tag-link">Skyscrapper</a>
                                    <a href="#" class="tag-link">Constructions</a>
                                    <a href="#" class="tag-link">Home</a>
                                    <a href="#" class="tag-link">Renovation</a>
                                    <a href="#" class="tag-link">Interior Design</a>
                                    <a href="#" class="tag-link">Exterior</a>
                                    <a href="#" class="tag-link">Painting</a>
                                </div><!-- /.widget-details -->
                            </div><!-- /.widget -->

                            <div class="widget widget_twitter_feed">
                                <h3 class="widget-title">Twitter</h3><!-- /.widget-title -->
                                <div class="widget-details">
                                    <div id="tweet-slider" class="tweet-slider carousel slide">
                                        <ol class="carousel-indicators">
                                            <li data-target="#tweet-slider" data-slide-to="0" class="active"></li>
                                            <li data-target="#tweet-slider" data-slide-to="1"></li>
                                            <li data-target="#tweet-slider" data-slide-to="2"></li>
                                        </ol>
                                        <div class="carousel-inner">
                                            <div class="item active">
                                                How to Do Away With Duplicate Content Of WordPress <a href="https://t.co/NMDURh1uK2">https://t.co/NMDURh1uK2</a> <a href="https://t.co/vMxfnaPoV0">https://t.co/vMxfnaPoV0</a>
                                                <span class="date">Dec/6/2016</span>
                                            </div>

                                            <div class="item">
                                                Huge WordPress Deals For Black Friday & Cyber Monday 2016 <a href="https://t.co/yYzEhdIEdv">https://t.co/yYzEhdIEdv</a>
                                                <span class="date">Nov/26/2016</span>
                                            </div>

                                            <div class="item">
                                                Huge HTML5 Template Deals For Black Friday & Cyber Monday 2016 <a href="https://t.co/yYzEhdIEdv">https://t.co/yYzEhdIEdv</a>
                                                <span class="date">Nov/25/2016</span>
                                            </div>
                                        </div><!-- /.carousel-inner -->
                                    </div><!-- /#tweet-slider -->
                                </div><!-- /.widget-details -->
                            </div><!-- /.widget -->
                        </aside><!-- /.sidebar -->
                    </div>
                </div><!-- /.items -->
            </div><!-- /.container -->
        </div><!-- /.section-padding -->
    </section><!-- /.blog-posts -->



    <section class="related-post">
        <div class="section-padding">
            <div class="container">
                <div class="inner-bg">
                    <h2 class="section-title">Related News</h2><!-- /.section-title -->

                    <div class="padding">
                        <div class="items">
                            <div id="related-post-slider" class="related-post-slider owl-carousel owl-theme">
                                <div class="item">
                                    <article class="post type-post">
                                        <div class="entry-thumbnail"><img src="images/post/14.jpg" alt="Entry Thumbnail"></div><!-- /.entry-thumbnail -->
                                        <div class="entry-content">
                                            <h3 class="entry-title"><a href="single.html">Marla Kraft earns real estate designation</a></h3><!-- /.entry-title -->
                                            <div class="entry-meta">
                                                <span class="time"><time datetime="2016-11-28">Nov 28, 2016</time></span>
                                                <span class="author">by <a href="#">Anthony</a></span>
                                            </div><!-- /.entry-meta -->
                                            <p class="description">
                                                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud
                                            </p><!-- /.description -->
                                            <a href="single.html" class="btn read-more">Read more</a><!-- /.btn -->
                                        </div><!-- /.entry-content -->
                                    </article><!-- /.post.type-post -->
                                </div><!-- /.item -->

                                <div class="item">
                                    <article class="post type-post">
                                        <div class="entry-thumbnail"><img src="images/post/15.jpg" alt="Entry Thumbnail"></div><!-- /.entry-thumbnail -->
                                        <div class="entry-content">
                                            <h3 class="entry-title"><a href="single.html">Greenland HK raises $300 mln for real estate fund with Kuwait</a></h3><!-- /.entry-title -->
                                            <div class="entry-meta">
                                                <span class="time"><time datetime="2016-11-28">Nov 28, 2016</time></span>
                                                <span class="author">by <a href="#">Anthony</a></span>
                                            </div><!-- /.entry-meta -->
                                            <p class="description">
                                                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud
                                            </p><!-- /.description -->
                                            <a href="single.html" class="btn read-more">Read more</a><!-- /.btn -->
                                        </div><!-- /.entry-content -->
                                    </article><!-- /.post.type-post -->
                                </div><!-- /.item -->

                                <div class="item">
                                    <article class="post type-post">
                                        <div class="entry-thumbnail"><img src="images/post/16.jpg" alt="Entry Thumbnail"></div><!-- /.entry-thumbnail -->
                                        <div class="entry-content">
                                            <h3 class="entry-title"><a href="single.html">Trump talks Treasury with Blackstone real estate boss</a></h3><!-- /.entry-title -->
                                            <div class="entry-meta">
                                                <span class="time"><time datetime="2016-11-28">Nov 28, 2016</time></span>
                                                <span class="author">by <a href="#">Anthony</a></span>
                                            </div><!-- /.entry-meta -->
                                            <p class="description">
                                                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud
                                            </p><!-- /.description -->
                                            <a href="single.html" class="btn read-more">Read more</a><!-- /.btn -->
                                        </div><!-- /.entry-content -->
                                    </article><!-- /.post.type-post -->
                                </div><!-- /.item -->

                                <div class="item">
                                    <article class="post type-post">
                                        <div class="entry-thumbnail"><img src="images/post/4.jpg" alt="Entry Thumbnail"></div><!-- /.entry-thumbnail -->
                                        <div class="entry-content">
                                            <h3 class="entry-title"><a href="single.html">Marla Kraft earns real estate designation</a></h3><!-- /.entry-title -->
                                            <div class="entry-meta">
                                                <span class="time"><time datetime="2016-11-28">Nov 28, 2016</time></span>
                                                <span class="author">by <a href="#">Anthony</a></span>
                                            </div><!-- /.entry-meta -->
                                            <p class="description">
                                                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud
                                            </p><!-- /.description -->
                                            <a href="single.html" class="btn read-more">Read more</a><!-- /.btn -->
                                        </div><!-- /.entry-content -->
                                    </article><!-- /.post.type-post -->
                                </div><!-- /.item -->

                                <div class="item">
                                    <article class="post type-post">
                                        <div class="entry-thumbnail"><img src="images/post/5.jpg" alt="Entry Thumbnail"></div><!-- /.entry-thumbnail -->
                                        <div class="entry-content">
                                            <h3 class="entry-title"><a href="single.html">Greenland HK raises $300 mln for real estate fund with Kuwait</a></h3><!-- /.entry-title -->
                                            <div class="entry-meta">
                                                <span class="time"><time datetime="2016-11-28">Nov 28, 2016</time></span>
                                                <span class="author">by <a href="#">Anthony</a></span>
                                            </div><!-- /.entry-meta -->
                                            <p class="description">
                                                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud
                                            </p><!-- /.description -->
                                            <a href="single.html" class="btn read-more">Read more</a><!-- /.btn -->
                                        </div><!-- /.entry-content -->
                                    </article><!-- /.post.type-post -->
                                </div><!-- /.item -->

                                <div class="item">
                                    <article class="post type-post">
                                        <div class="entry-thumbnail"><img src="images/post/6.jpg" alt="Entry Thumbnail"></div><!-- /.entry-thumbnail -->
                                        <div class="entry-content">
                                            <h3 class="entry-title"><a href="single.html">Trump talks Treasury with Blackstone real estate boss</a></h3><!-- /.entry-title -->
                                            <div class="entry-meta">
                                                <span class="time"><time datetime="2016-11-28">Nov 28, 2016</time></span>
                                                <span class="author">by <a href="#">Anthony</a></span>
                                            </div><!-- /.entry-meta -->
                                            <p class="description">
                                                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud
                                            </p><!-- /.description -->
                                            <a href="single.html" class="btn read-more">Read more</a><!-- /.btn -->
                                        </div><!-- /.entry-content -->
                                    </article><!-- /.post.type-post -->
                                </div><!-- /.item -->
                            </div><!-- /#latest-post-slider -->
                        </div><!-- /.items -->
                    </div><!-- /.padding -->
                </div><!-- /.inner-bg -->
            </div><!-- /.container -->
        </div><!-- /.section-padding -->
    </section><!-- /.related-post -->


    <footer class="site-footer">
        @include('user1.template.footer')
    </footer><!-- /.footer-bottom -->


    <script src="{{asset('cast/assets/js/plugins.js')}}"></script>
    <script src="{{asset('cast/assets/js/main.js')}}"></script>

</body>

<!-- Mirrored from demos.jeweltheme.com/cast/blog-single.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 30 Jul 2019 21:12:32 GMT -->
</html>
