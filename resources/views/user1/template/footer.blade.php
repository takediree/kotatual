<div class="footer-top">
            <div class="section-padding">
                <div class="container">
                    <div class="inner-bg">
                        <div class="padding">
                            <div class="col-sm-3">
                                <div class="widget widget_contacts">
                                    <h3 class="widget-title">Kontak</h3><!-- /.widget-title -->
                                    <div class="widget-details">
                                        <address>
                                            <span>
                                                <i class="fa fa-map-marker"></i>
                                                <span>
                                                Desa Dullah, Kec. P. Dullah Utara, 97613, Dullah Darat, North Dullah Island, Tual City, Maluku
                                                </span>
                                            </span>
                                            <span>
                                                <i class="fa fa-envelope-square"></i>
                                                <span>
                                                    +61 3 8376 6284
                                                </span>
                                            </span>
                                            <span>
                                                <i class="fa fa-phone-square"></i>
                                                <span>
                                                    <a href="#">contact@cast.com</a>
                                                </span>
                                            </span>
                                        </address>
                                    </div><!-- /.widget-details -->
                                </div><!-- /.widget -->
                            </div>

                            <div class="col-sm-3">
                                <div class="widget widget_info">
                                    <h3 class="widget-title">Company Info</h3><!-- /.widget-title -->
                                    <div class="widget-details">
                                        <a href="http://127.0.0.1:8000/user1/about"><i class="ti-arrow-right"></i>Visi Misi</a>
                                        
                                        <a href="http://127.0.0.1:8000/user1/struktur"><i class="ti-arrow-right"></i>Struktur</a>
                                        <a href="http://127.0.0.1:8000/user1/map"><i class="ti-arrow-right"></i>Map</a>
                                      
                                        
                                    </div><!-- /.widget-details -->
                                </div><!-- /.widget -->
                            </div>

                            <div class="col-sm-3">
                                <div class="widget widget_office_hours">
                                    <h3 class="widget-title">Office Hours</h3><!-- /.widget-title -->
                                    <div class="widget-details">
                                        <p>

                                            support 24 jam dalam tujuh hari
                                        </p>
                                        <div class="time">
                                            <span><span class="meta-id">Minggu - Jumat:</span> 10:00am to
                                                08:00pm</span>
                                            <span><span class="meta-id">Weekend:</span> 12:00am to 07:00pm</span>
                                        </div>
                                    </div><!-- /.widget-details -->
                                </div><!-- /.widget -->
                            </div>

                            <div class="col-sm-3">
                                <div class="widget widget_subscribe">
                                    <h3 class="widget-title">Saran</h3><!-- /.widget-title -->
                                    <div class="widget-details">
                                        <p>
                                          <a href="http://127.0.0.1:8000/user1/saran">klik</a> Masukkan saran
                                        </p>
                                       
                                    </div><!-- /.widget-details -->
                                </div><!-- /.widget -->
                            </div>
                        </div><!-- /.padding -->
                    </div><!-- /.inner-bg -->
                </div><!-- /.container -->
            </div><!-- /.section-padding -->
        </div><!-- /.footer-top -->

        <div class="footer-bottom">
            <div class="container">
                <div class="copyright text-left">
                    <div class="padding">
                        © <a href="demos.jeweltheme.com/cast.html">Webgis</a> 2020 | Develpoed With ❤ by <a
                            href="https://jeweltheme.com/">tdr</a>
                        <span class="pull-right">Designed by <a href="#">tdr</a></span>
                    </div><!-- /.padding -->
                </div><!-- /.copyright -->
            </div><!-- /.container -->
        </div><!-- /.footer-bottom -->