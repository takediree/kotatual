<aside class="sidebar">
                            <div class="widget widget_search">
                                <div class="widget-details">
                                    <form method="get" class="search-form" action="#">
                                        <input type="text" class="form-control" placeholder="Search" name="search" title="Search.." required="">
                                        <input type="submit" class="form-control" name="submit">
                                    </form>
                                </div><!-- /.widget-details -->
                            </div><!-- /.widget -->

                            <div class="widget widget_categories">
                                <h3 class="widget-title">DUllah Utara</h3><!-- /.widget-title -->
                                <div class="widget-details">
                                    @foreach($desa as $desaa)

                                        <a href="#">{{$desaa->nama_desa}}</a>
                                        
                                    @endforeach

                                </div><!-- /.widget-details -->
                            </div><!-- /.widget -->

                            
                            <section class="contact-details">
                            <h2 class="section-title">Kontact info</h2><!-- /.section-title -->
                            <div class="padding">
                                <div class="item media">
                                    <div class="item-icon media-left"><i class="ti-location-pin"></i></div><!-- /.item-icon -->
                                    <div class="item-details media-body">
                                        <h3 class="item-title">Alamat</h3><!-- /.item-title -->
                                        <span>
                                        Desa Dullah, Kec. P. Dullah Utara, 97613, Dullah Darat, North Dullah Island, Tual City, Maluku
                                        </span>
                                    </div><!-- /.item-details -->
                                </div><!-- /.item -->

                                

                                <div class="item media">
                                    <div class="item-icon media-left"><i class="ti-email"></i></div><!-- /.item-icon -->
                                    <div class="item-details media-body">
                                        <h3 class="item-title">Email</h3><!-- /.item-title -->
                                        <span>
                                                webgis@gmail.com
                                        </span>
                                    </div><!-- /.item-details -->
                                </div><!-- /.item -->
                            </div><!-- /.padding -->
    </section><!-- /.contact-details -->                       
                        </aside><!-- /.sidebar -->

                     