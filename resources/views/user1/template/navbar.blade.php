            <div class="container">
                <nav class="navbar navbar-default">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#menu"
                            aria-expanded="false">
                            <i class="fa fa-bars"></i>
                        </button>
                        <!-- <a class="navbar-brand" href="index-02.html"><img src="images/LOGO1.png" alt="Logo"></a> -->
                    </div>

                    <div id="menu" class="main-menu collapse navbar-collapse pull-right">

                        <ul class="nav navbar-nav">

                            <li class="#"><a href="{{url('/')}}">Home</a></li>

                            <li class=""> <a href="{{url('user1/about')}}">Visi Misi</a></li>
                            
                            <li class=""> <a href="{{url('user1/struktur')}}">Struktur</a></li>
                            
                            <li class="menu-item-has-children">
                                <a href="#">Desa</a>
                                <ul class="sub-menu children">
                                    
                                @foreach($desa as $d)
                                    <li><a href="{{ url('http://127.0.0.1:8000/user1/desa?id='.$d->id) }}">{{ $d->nama_desa }}</a></li>

                                    @endforeach
                                </ul>
                            </li>


                            <li> <a href="{{url('user1/map')}}">Map</a></li>

                            <li><a href="http://127.0.0.1:8000/user1/saran">Saran    </a></li>

                        </ul>

                        <div class="search-box">
                            <span class="search-icon"><i class="ti-search"></i></span><!-- /.search-icon -->
                            <form class="search-form" method="get" action="#">
                                <input name="s" type="text" class="search-input" size="20" maxlength="20"
                                    placeholder="Search Here ...." required="">
                                <input type="submit" name="submit" class="search-submit">
                            </form><!-- /.search-form -->
                        </div>
                    </div><!-- /.navbar-collapse -->
                </nav><!-- /.navbar -->
            </div><!-- /.container -->