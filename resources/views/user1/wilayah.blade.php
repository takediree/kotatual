<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="zxx"> <!--<![endif]-->

<!-- Mirrored from demos.jeweltheme.com/cast/blog-02.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 30 Jul 2019 21:12:24 GMT -->
<head>
 
    @include('user1.template.head')

</head>


<body>



    <header class="header">
        <div class="header-top gray-bg">
           @include('user1.template.header')
        </div><!-- /.header-top -->

        <div class="header-bottom">
            @include('user1.template.navbar')
        </div><!-- /.header-bottom -->
    </header><!-- /.header -->




    <section class="page-name">
        <div class="section-padding">
            <div class="container">
                <div class="name-section background-bg" data-image-src="images/bg7.jpg">
                    <div class="overlay">
                        <div class="padding">
                            <h2 class="page-title">Potensi Wilayah Desa</h2><!-- /.page-ttile -->
                            <ol class="breadcrumb">
                                <li><a href="#">Home</a></li>
                                <li class="active">News</li>
                            </ol>
                        </div><!-- /.padding -->
                    </div><!-- /.overlay -->
                </div><!-- /.name-section -->
            </div><!-- /.container -->
        </div><!-- /.section-padding -->
    </section><!-- /.page-name -->


                    <section id="google-map">
                        <div class="section-padding">
                            <div class="container">
                                <div class="map-container ">
                      
                                   <div id="map" style="width:100%; height:400px;"></div>

                                </div><!-- /.map-container -->
                            </div><!-- /.container -->
                        </div><!-- /.section-padding -->
                    </section><!-- /#google-map-->


    <section class="blog-posts tile-layout">
        <div class="section-padding">
            <div class="container">
                <div class="items">
                    <div class="col-sm-8">

                    @foreach($potensiWilayah as $wil)

                        <article class="post type-post">
                            <div class="col-md-6">
                                <div class="entry-thumbnail"><img src="{{asset('uploads/'.$wil->gambar) }}" alt="Entry Thumbnail"></div><!-- /.entry-thumbnail -->
                            </div>
                            <div class="col-md-6">
                                <div class="entry-content">
                                    <h3 class="entry-title"><a href="blog-single.html">{{$wil->nama}} </a></h3><!-- /.entry-title -->
                                    <div class="entry-meta">
                                        <span><time datetime="2016-11-15">Nov 16, 2020</time></span>
                                        <span>Posted by <a href="#">Admin</a></span>
                                        <!-- <span>with <a href="#">17 Comments</a></span> -->
                                    </div><!-- /.entry-meta -->
                                    <p class="description">
                                       {{$wil->keterangan}}
                                    </p><!-- /.description -->
                                    <!-- <a href="#" class="btn read-more">Read more <i class="ti-arrow-right"></i></a> -->
                                    <!-- /.btn -->
                                </div><!-- /.entry-content -->
                            </div>
                        </article><!-- /.post -->
                        
                    @endforeach


                        <div class="pagination">
                            <a href="#" class="prev"><i class="ti-arrow-left"></i></a>
                            <a href="#" class="active">1</a>
                            <a href="#">2</a>
                            <a href="#">3</a>
                            <a href="#">4</a>
                            <a href="#" class="next"><i class="ti-arrow-right"></i></a>
                        </div><!-- /.pagination -->
                    </div>

                    <div class="col-sm-4">
                        <aside class="sidebar">
                           @include('user1.template.sidebar')
                        </aside><!-- /.sidebar -->
                    </div>
                </div><!-- /.items -->
            </div><!-- /.container -->
        </div><!-- /.section-padding -->
    </section><!-- /.blog-posts -->




   

    <footer class="site-footer">
        @include('user1.template.footer')
    </footer><!-- /.footer-bottom -->


    <script src="{{asset('cast/assets/js/plugins.js')}}"></script>
    <script src="{{asset('cast/assets/js/main.js')}}"></script>


</body>

<!-- Mirrored from demos.jeweltheme.com/cast/blog-02.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 30 Jul 2019 21:12:28 GMT -->
</html>
