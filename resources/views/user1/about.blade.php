<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="zxx"> <!--<![endif]-->

<!-- Mirrored from demos.jeweltheme.com/cast/about.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 30 Jul 2019 21:12:32 GMT -->
<head>
 
    @include('user1.template.head')

</head>

<body>



    <header class="header">
        <div class="header-top gray-bg">
           @include('user1.template.header')
        </div><!-- /.header-top -->

        <div class="header-bottom">
            @include('user1.template.navbar')
        </div><!-- /.header-bottom -->
    </header><!-- /.header -->




    <section class="page-name">
        <div class="section-padding">
            <div class="container">
                <div class="name-section background-bg" data-image-src="images/bg2.jpg">
                    <div class="overlay">
                        <div class="padding">
                            <h2 class="page-title">Visi dan Misi</h2><!-- /.page-ttile -->
                            <ol class="breadcrumb">
                                <li><a href="#">Home</a></li>
                                <li class="active">Visi Misi</li>
                            </ol>
                        </div><!-- /.padding -->
                    </div><!-- /.overlay -->
                </div><!-- /.name-section -->
            </div><!-- /.container -->
        </div><!-- /.section-padding -->
    </section><!-- /.page-name -->




    <section class="about about-5">
        <div class="section-padding">
            <div class="container">
                <div class="items">
                  

                    <div class="col-sm-6">
                        <div class="inner-bg">
                            <h2 class="section-title">Visi dan  <span>Misi</span> Kecamatan Dullah Utara</h2><!-- /.section-title -->
                            <div class="padding">
                                <h2 class="section-title"> Visi</h2>
                                <p class="description">
                                    <strong>
                                    Dalam rangka mewujudkan kegiatan pembangunan untuk mendukung Visi Kota Tual pada Tahun 2016-2021, maka Kecamatan Dullah Utara telah merumuskan Visi Pembangunan sama dengan Visi Kabupaten Bantul yaitu :
                                    </strong>
                                    <br><br>
                                    Terwujudnya masyarakat Kabupaten Bantul  yang sehat,cerdas,dan sejahtera berdasarkan nilai-nilai keagamaan,kemanusiaan dan kebangsaan,dalam wadah Negara kesatuan Ripublik Indonesia (NKRI)
                                    <br><br>
                                <h2 class="section-title"> Misi</h2>
                                <p class="description">
                                    <strong>
                                    Misi merupakan perwujudan tujuan operasi Organisasi Pemerintahan dalam melayani masyarakat, sehingga setiap saat perlu dilakukan perubahan sesuai dengan perubahan jaman. Sebagai penjabaran dari Visi yang telah ditetapkan di atas, Misi merupakan uraian kegiatan yang akan dilaksanakan untuk mencapai tujuan yang diharapkan dari pencanangan Visi tersebut.
                                    </strong>
                                    <br><br>
                                    Adapun Misi Kecamatan Dullah Utara adalah sebagai berikut :

                    Meningkatkan tata kelola pemerintahan  yang baik,efektif,evisien dan bebas dari KKN melalui percepatan birikrasi reformasi
                                    <br><br>
                                    
                                    
                                </p><!-- /.description -->
                            </div>                           
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="item">
                            <div class="inner-bg">
                                <div class="item-thumb">
                                   
                                    <img src="{{url('venue/img/Tuall.jpeg')}}" alt="Image">
                                    <div class="padding">
                                         <p class="description">
                                         Desa yang termasuk ke dalam kecamatan ini yaitu:
                                                                                        Dullah,
                                                                                        Dullah Laut,
                                                                                        Fiditan,
                                                                                        Lebetawi,
                                                                                        Ohoitel,
                                                                                        Ohoitahit,
                                                                                        Tamedan,
                                                                                        Ngadi,
                                    
                                    
                                </p><!-- /.description -->
                            </div>     
                                   
                                </div>
                                <!-- /.item-thumb -->
                                
                            </div><!-- /.inner-bg -->
                        </div><!-- /.item -->
                    </div>
                </div><!-- /.items -->
            </div><!-- /.container -->
        </div><!-- /.section-padding -->
    </section><!-- /.about-5 -->




   



    <section class="team-section">
        <div class="section-padding">
            <div class="container">
                <div class="inner-bg">
                    <h2 class="section-title">Pengurus Kecamatan</h2>
                    <div class="padding">
                        
                        <div class="items">
                            <div id="team-slider" class="team-slider owl-carousel owl-theme text-center">
                                @foreach($pengurus as $p)
                                <div class="item">
                                    <div class="member">
                                        <div class="member-image"><img src="images/team/1.png" alt="Member Image"></div><!-- /.member-image -->
                                        <div class="member-details">
                                            <h3 class="name item-title"><a href="#">{{$p->nama}}</a></h3><!-- /.name -->
                                            <span class="designation">{{ $p->jabatan }}</span><!-- /.designation -->
                                            <div class="member-social">
                                                <a href="#"><i class="fa fa-facebook"></i></a>
                                                <a href="#"><i class="fa fa-twitter"></i></a>
                                                <a href="#"><i class="fa fa-google-plus-circle"></i></a>
                                            </div><!-- /.member-social -->
                                        </div><!-- /.member-details -->
                                    </div><!-- /.member -->
                                </div><!-- /.item -->
                                @endforeach
                                
                            </div><!-- /#team-slider -->
                        </div><!-- /.items -->
                    </div><!-- /.padding -->
                </div><!-- /.inner-bg -->
            </div><!-- /.container -->
        </div><!-- /.section-padding -->
    </section><!-- /.team-section -->












    <footer class="site-footer">
        @include('user1.template.footer')
    </footer><!-- /.footer-bottom -->


    <script src="{{asset('cast/assets/js/plugins.js')}}"></script>
    <script src="{{asset('cast/assets/js/main.js')}}"></script>

</body>

<!-- Mirrored from demos.jeweltheme.com/cast/about.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 30 Jul 2019 21:12:38 GMT -->
</html>
