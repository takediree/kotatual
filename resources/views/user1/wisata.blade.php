<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="zxx"> <!--<![endif]-->

<!-- Mirrored from demos.jeweltheme.com/cast/blog-02.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 30 Jul 2019 21:12:24 GMT -->
<head>
 
    @include('user1.template.head')

</head>


<body>



    <header class="header">
        <div class="header-top gray-bg">
           @include('user1.template.header')
        </div><!-- /.header-top -->

        <div class="header-bottom">
            @include('user1.template.navbar')
        </div><!-- /.header-bottom -->
    </header><!-- /.header -->




    <section class="page-name">
        <div class="section-padding">
            <div class="container">
                <div class="name-section background-bg" data-image-src="images/bg7.jpg">
                    <div class="overlay">
                        <div class="padding">
                            <h2 class="page-title">Potensi Wisata Desa</h2><!-- /.page-ttile -->
                            <ol class="breadcrumb">
                                <li><a href="#">potensi wilayah desa</a></li>
                                <li class="active"></li>
                            </ol>
                        </div><!-- /.padding -->
                    </div><!-- /.overlay -->
                </div><!-- /.name-section -->
            </div><!-- /.container -->
        </div><!-- /.section-padding -->
    </section><!-- /.page-name -->


                    <section id="google-map">
                        <div class="section-padding">
                            <div class="container">
                                <div class="map-container ">
                      
                                   <div id="map" style="width:100%; height:400px;"></div>

                                </div><!-- /.map-container -->
                            </div><!-- /.container -->
                        </div><!-- /.section-padding -->
                    </section><!-- /#google-map-->

    <section class="blog-posts tile-layout">
        <div class="section-padding">
            <div class="container">
                <div class="items">
                    <div class="col-sm-8">

                    @foreach($potensiWisata as $w)

                        <article class="post type-post">
                            <div class="col-md-6">
                                <div class="entry-thumbnail"><img src="{{asset('uploads/'.$w->gambar) }}" alt="Entry Thumbnail"></div><!-- /.entry-thumbnail -->
                            </div>
                            <div class="col-md-6">
                                <div class="entry-content">
                                    <h3 class="entry-title">{{$w->nama_wisata}} </h3><!-- /.entry-title -->
                                    <div class="entry-meta">
                                        <span><time datetime="{{$w->created_at}}">{{$w->created_at}}</time></span>
                                        <span>Posted by <a href="#">Admin</a></span>
                                        <!-- <span>with <a href="#">17 Comments</a></span> -->
                                    </div><!-- /.entry-meta -->
                                    <p class="description">
                                       {{$w->keterangan}}
                                    </p><!-- /.description -->
                                    <!-- <a href="#" class="btn read-more">Read more <i class="ti-arrow-right"></i></a> -->
                                    <!-- /.btn -->
                                </div><!-- /.entry-content -->
                            </div>
                        </article><!-- /.post -->
                        
                    @endforeach


                        <div class="pagination">
                            <a href="#" class="prev"><i class="ti-arrow-left"></i></a>
                            <a href="#" class="active">1</a>
                            <a href="#">2</a>
                            <a href="#">3</a>
                            <a href="#">4</a>
                            <a href="#" class="next"><i class="ti-arrow-right"></i></a>
                        </div><!-- /.pagination -->
                    </div>

                    <div class="col-sm-4">
                        <aside class="sidebar">
                           @include('user1.template.sidebar')
                        </aside><!-- /.sidebar -->
                    </div>
                </div><!-- /.items -->
            </div><!-- /.container -->
        </div><!-- /.section-padding -->
    </section><!-- /.blog-posts -->


                   


   

    <footer class="site-footer">
        @include('user1.template.footer')
    </footer><!-- /.footer-bottom -->


    <script src="{{asset('cast/assets/js/plugins.js')}}"></script>
    <script src="{{asset('cast/assets/js/main.js')}}"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD2jlT6C_to6X1mMvR9yRWeRvpIgTXgddM"></script>
    <script src="https://polyfill.io/v3/polyfill.min.js?features=default"></script>
    <script type="text/javascript">

        jQuery(document).ready(function($) {


            

            var locationsWisata = [];

            @foreach($potensiWisata as $d)


                var location = ['{{ $d->nama_wisata}}', {{ $d->longitude }}, {{ $d->latitude }}];

                locationsWisata.push(location);
                            
            @endforeach

                var map = new google.maps.Map(document.getElementById('map'), {
                zoom: 10,
                center: new google.maps.LatLng(-5.611030,132.783940),
                mapTypeId: google.maps.MapTypeId.ROADMAP
                });

                var infowindow = new google.maps.InfoWindow();

                var marker, i;

               

                for (i = 0; i < locationsWisata.length; i++) {  
                marker = new google.maps.Marker({
                    position: new google.maps.LatLng(locationsWisata[i][1], locationsWisata[i][2]),
                    map: map
                });
              
                    google.maps.event.addListener(marker, 'click', (function(marker, i) {
                        return function() {
                        infowindow.setContent(locationsWisata[i][0]);
                        infowindow.open(map, marker);
                        }   
                    })(marker, i));

                }

             



        });

    </script>


</body>

<!-- Mirrored from demos.jeweltheme.com/cast/blog-02.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 30 Jul 2019 21:12:28 GMT -->
</html>
