<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="zxx"> <!--<![endif]-->

<!-- Mirrored from demos.jeweltheme.com/cast/contact.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 30 Jul 2019 21:12:47 GMT -->
<head>
  @include('user1.template.head')
</head>


<body>



    <header class="header">
        <div class="header-top gray-bg">
           @include('user1.template.header')
        </div><!-- /.header-top -->

        <div class="header-bottom">
            @include('user1.template.navbar')
        </div><!-- /.header-bottom -->
    </header><!-- /.header -->



    <section class="page-name">
        <div class="section-padding">
            <div class="container">
                <div class="name-section background-bg" data-image-src="images/bg.jpg">
                    <div class="overlay">
                        <div class="padding">
                            <h2 class="page-title">Saran</h2><!-- /.page-ttile -->
                            <ol class="breadcrumb">
                                <li><a href="#">Home</a></li>
                                <li class="active">Saran</li>
                            </ol>
                        </div><!-- /.padding -->
                    </div><!-- /.overlay -->
                </div><!-- /.name-section -->
            </div><!-- /.container -->
        </div><!-- /.section-padding -->
    </section><!-- /.page-name -->




    <section class="contact-details">
        <div class="section-padding">
            <div class="container">
                <div class="items">
                    <div class="col-sm-8">
                        <div class="inner-bg">
                            <h2 class="section-title"></h2><!-- /.section-title -->
                            <div class="padding">

                                <form action="/user1/saran" method="post" class="wpcf7-form contact-form">
                                    @csrf
                                    <input type="text" id="name" name="nama" value="" class="wpcf7-form-control" placeholder="Masukkan Nama" required="">

                                    <input type="email" id="email" name="email" value="" class="wpcf7-form-control" placeholder="Masukkan Email" required="">

                                    <textarea id="saran" name="saran" class="wpcf7-form-control" placeholder="Masukkan Saran Anda" required=""></textarea>

                                    <button type="submit" class="wpcf7-form-control btn">Kirim Saran</button>
                                   
                                    <div class="contact-message"></div>
                                    <div class="error-message"></div>
                                </form>
                                @if (session('status'))

                                    <div class="alert alert-success col-md-4">
                                        {{session('status')}}
                                    </div>

                                @endif
                            </div><!-- /.padding -->
                        </div><!-- /.inner-bg -->
                    </div>

                    <div class="col-sm-4">
                        <div class="inner-bg">
                            <h2 class="section-title">Kontact info</h2><!-- /.section-title -->
                            <div class="padding">
                                <div class="item media">
                                    <div class="item-icon media-left"><i class="ti-location-pin"></i></div><!-- /.item-icon -->
                                    <div class="item-details media-body">
                                        <h3 class="item-title">Alamat</h3><!-- /.item-title -->
                                        <span>
                                        Desa Dullah, Kec. P. Dullah Utara, 97613, Dullah Darat, North Dullah Island, Tual City, Maluku
                                        </span>
                                    </div><!-- /.item-details -->
                                </div><!-- /.item -->

                                <div class="item media">
                                    <div class="item-icon media-left"><i class="ti-mobile"></i></div><!-- /.item-icon -->
                                    <div class="item-details media-body">
                                        <h3 class="item-title">Telepon</h3><!-- /.item-title -->
                                        <span>
                                            +61 3 8376 6284 (10 AM - 07 PM)
                                        </span>
                                        <span>
                                            +61 3 8376 6284 (10 AM - 07 PM)
                                        </span>
                                    </div><!-- /.item-details -->
                                </div><!-- /.item -->

                                <div class="item media">
                                    <div class="item-icon media-left"><i class="ti-email"></i></div><!-- /.item-icon -->
                                    <div class="item-details media-body">
                                        <h3 class="item-title">Email</h3><!-- /.item-title -->
                                        <span>
                                                webgis@gmail.com
                                        </span>
                                    </div><!-- /.item-details -->
                                </div><!-- /.item -->
                            </div><!-- /.padding -->
                        </div><!-- /.inner-bg -->
                    </div>
                </div><!-- /.items -->
            </div><!-- /.container -->
        </div><!-- /.section-padding -->
    </section><!-- /.contact-details -->






   



    <footer class="site-footer">
        @include('user1.template.footer')
    </footer><!-- /.footer-bottom -->








</body>

<!-- Mirrored from demos.jeweltheme.com/cast/contact.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 30 Jul 2019 21:12:47 GMT -->
</html>
