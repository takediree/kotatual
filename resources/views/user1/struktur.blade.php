<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="zxx"> <!--<![endif]-->

<!-- Mirrored from demos.jeweltheme.com/cast/team.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 30 Jul 2019 21:12:38 GMT -->
<head>
  
    @include('user1.template.head')

</head>


<body>


<header class="header">
        <div class="header-top gray-bg">
           @include('user1.template.header')
        </div><!-- /.header-top -->

        <div class="header-bottom">
            @include('user1.template.navbar')
        </div><!-- /.header-bottom -->
    </header><!-- /.header -->



    <section class="page-name">
        <div class="section-padding">
            <div class="container">
                <div class="name-section background-bg" data-image-src="images/bg5.jpg">
                    <div class="overlay">
                        <div class="padding">
                            <h2 class="page-title">Struktur</h2><!-- /.page-ttile -->
                            <ol class="breadcrumb">
                                <li><a href="#">Home</a></li>
                                
                                <li class="active">Struktur</li>
                            </ol>
                        </div><!-- /.padding -->
                    </div><!-- /.overlay -->
                </div><!-- /.name-section -->
            </div><!-- /.container -->
        </div><!-- /.section-padding -->
    </section><!-- /.page-name -->




    <section class="our-team">
        <div class="section-padding">
            <div class="container">
                <div class="items">

                @foreach($pengurus as $p)

                    <div class="col-sm-3">
                        <div class="member">
                            <div class="inner-bg">
                                <div class="member-image"><img src="{{ asset('uploads/'.$p->gambar) }}" alt="Member Image"></div><!-- /.member-image -->
                                <div class="member-details">
                                    <div class="padding">
                                        <h3 class="name">{{$p->nama}}</h3><!-- /.name -->
                                        <span class="designation">{{$p->jabatan}}</span><!-- /.designation -->
                                        <!-- <p class="description">
                                            Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est
                                        </p> -->
                                        <!-- /.description -->
                                        <div class="member-social">
                                            <a href=# class="fa fa-facebook"></i></a>
                                            <a href="#"><i class="fa fa-twitter"></i></a>
                                            <a href="#"><i class="fa fa-google-plus-circle"></i></a>
                                        </div><!-- /.member-social -->
                                    </div><!-- /.padding -->
                                </div><!-- /.member-details -->
                            </div><!-- /.inner-bg -->
                        </div><!-- /.member -->
                    </div>

                  @endforeach

                </div><!-- /.items -->
            </div><!-- /.container -->
        </div><!-- /.section-padding -->
    </section><!-- /.our-team -->    



   




    <footer class="site-footer">
        @include('user1.template.footer')
    </footer><!-- /.footer-bottom -->


    <script src="{{asset('cast/assets/js/plugins.js')}}"></script>
    <script src="{{asset('cast/assets/js/main.js')}}"></script>


</body>

<!-- Mirrored from demos.jeweltheme.com/cast/team.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 30 Jul 2019 21:12:39 GMT -->
</html>
