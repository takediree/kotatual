<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="zxx"> <!--<![endif]-->

<!-- Mirrored from demos.jeweltheme.com/cast/blog-single.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 30 Jul 2019 21:12:28 GMT -->
<head>
 
    @include('user1.template.head')

</head>


<body>

   <header class="header">
        <div class="header-top gray-bg">
           @include('user1.template.header')
        </div><!-- /.header-top -->

        <div class="header-bottom">
            @include('user1.template.navbar')
        </div><!-- /.header-bottom -->
    </header><!-- /.header -->

                 

    <section class="page-name">
        <div class="section-padding">
            <div class="container">
                <div class="name-section background-bg" data-image-src="images/bg7.jpg">
                    <div class="overlay">
                        <div class="padding">
                            <h2 class="page-title">Selamat Datang Di Desa {{ $detailDesa->nama_desa }} </h2><!-- /.page-ttile -->
                            <ol class="breadcrumb">
                                <li><a href="#">Home</a></li>
                                
                                <li class="active">{{ $detailDesa->nama_desa}}</li>
                            </ol>
                        </div><!-- /.padding -->
                    </div><!-- /.overlay -->
                </div><!-- /.name-section -->
            </div><!-- /.container -->
        </div><!-- /.section-padding -->
    </section><!-- /.page-name -->



                  

    
    <section class="blog-posts single-layout">
        <div class="section-padding">
            <div class="container">
                <div class="items">
                    <div class="col-sm-8">
                  
                            
                        <article class="post type-post">
                            <div class="entry-thumbnail"><img src="{{ asset('uploads/'.$detailDesa->gambar) }}" alt="Entry Thumbnail"></div><!-- /.entry-thumbnail -->
                            <br><br>
                            <div class="entry-content">
                                <h3 class="entry-title">{{ $detailDesa->nama_desa }}</h3><!-- /.entry-title -->
                                <div class="entry-meta">
                                    <span><time datetime="2016-11-15">Nov 15, 2016</time></span>
                                    <span>Posted by <a href="#">Anthony</a></span>
                                    <span>with <a href="#">17 Comments</a></span>
                                </div><!-- /.entry-meta -->

                                <div class="description">
                                    <p>
                                        {{$detailDesa->keterangan}}
                                    </p>

                                            </div><!-- /.description -->
                      
                                        </div><!-- /.entry-content -->

                                        
                        </article><!-- /.post -->
                        <div>
                        <br><br>
                        </div>
                        <aside class="sidebar">
                            <div class="widget widget_archives">
                            <div class="entry-content">
                                <h3 class="entry-title">Potensi Wilayah</h3><!-- /.widget-title -->
                                <div class="widget-details">
                                    <a href="{{url ('http://127.0.0.1:8000/user1/wisata?id='.$detailDesa->id)}}">Potensi Wisata</a>
                                    <a href="{{url ('http://127.0.0.1:8000/user1/sda?id='.$detailDesa->id)}}">Potensi Sumber Daya Alam</a>                                 
                                    <!-- <a href="">Potensi Wilayah</a> -->
                               
                                </div><!-- /.widget-details -->
                                </div>
                            </div><!-- /.widget --> 
                         </aside>
                
                         <section id="google-map">
                        <div class="section-padding">
                            <div class="container">
                                <div class="map-container ">
                      
                                   <div id="map" style="width:100%; height:400px;"></div>

                                </div><!-- /.map-container -->
                            </div><!-- /.container -->
                        </div><!-- /.section-padding -->
                    </section><!-- /#google-map-->
                    

                    </div>  

                    <div class="col-sm-4">
                        @include('user1.template.sidebar')
                        
                    </div>
                </div><!-- /.items -->
            </div><!-- /.container -->
        </div><!-- /.section-padding -->
    </section><!-- /.blog-posts -->


 
                 


    <footer class="site-footer">
        @include('user1.template.footer')
    </footer><!-- /.footer-bottom -->


    <script src="{{asset('cast/assets/js/plugins.js')}}"></script>
    <script src="{{asset('cast/assets/js/main.js')}}"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD2jlT6C_to6X1mMvR9yRWeRvpIgTXgddM"></script>
    <script src="https://polyfill.io/v3/polyfill.min.js?features=default"></script>
    <script type="text/javascript">

        jQuery(document).ready(function($) {


            var locationsWilayah = [];

            var locationsWisata = [];

            var locationsSumberdaya = [];
            
            
            @foreach($potensiWilayah as $d) 


                var location = ['{{ $d->nama}}', {{ $d->longitude }}, {{ $d->latitude }}];

                locationsWilayah.push(location);
                            
            @endforeach

                    
            @foreach($potensiWisata as $d)


                var location = ['{{ $d->nama_wisata}}', {{ $d->longitude }}, {{ $d->latitude }}];

                locationsWisata.push(location);
                            
            @endforeach

                    
            @foreach($potensiSda as $d)


                var location = ['{{ $d->nama}}', {{ $d->longitude }}, {{ $d->latitude }}];

                locationsSumberdaya.push(location);
                            
            @endforeach
            

                var map = new google.maps.Map(document.getElementById('map'), {
                zoom: 10,
                center: new google.maps.LatLng(-5.611030,132.783940),
                mapTypeId: google.maps.MapTypeId.ROADMAP
                });

                var infowindow = new google.maps.InfoWindow();

                var marker, i;

                for (i = 0; i < locationsWilayah.length; i++) {  
                marker = new google.maps.Marker({
                    position: new google.maps.LatLng(locationsWilayah[i][1], locationsWilayah[i][2]),
                    map: map
                });

                google.maps.event.addListener(marker, 'click', (function(marker, i) {
                        return function() {
                        infowindow.setContent(+locationsWilayah[i][0]+"<br>"+"Lat :"+locationsWilayah[i][1]);
                        infowindow.open(map, marker);
                        }
                    })(marker, i));

                }

                for (i = 0; i < locationsWisata.length; i++) {  
                marker = new google.maps.Marker({
                    position: new google.maps.LatLng(locationsWisata[i][1], locationsWisata[i][2]),
                    map: map
                });
              
                    google.maps.event.addListener(marker, 'click', (function(marker, i) {
                        return function() {
                        infowindow.setContent(locationsWisata[i][0]);
                        infowindow.open(map, marker);
                        }   
                    })(marker, i));

                }

                for (i = 0; i < locationsSumberdaya.length; i++) {  
                marker = new google.maps.Marker({
                    position: new google.maps.LatLng(locationsSumberdaya[i][1], locationsSumberdaya[i][2]),
                    map: map
                });

                google.maps.event.addListener(marker, 'click', (function(marker, i) {
                    return function() {
                    infowindow.setContent(locationsSumberdaya[i][0]);
                    infowindow.open(map, marker);
                    }
                })(marker, i));
                }



        });

    </script>


</body>

</html>
