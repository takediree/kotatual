<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSdaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sda', function (Blueprint $table) {
            $table->id();
            $table->string('nama');
            $table->string('desa');
            $table->string('longitude');
            $table->double('latitude');
            $table->string('keterangan');           
            $table->string('gambar')->nullable('true');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sda');
    }
}
